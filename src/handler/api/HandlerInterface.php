<?php
/**
 * Description :
 * This class allows to describe behavior of handler class.
 * Handler contains all information to handle specific errors,
 * to execute specific action.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\handler\api;



interface HandlerInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified error is catch by this handler.
     *
     * @param mixed $error
     * @return boolean
     */
    public function checkCatches($error);





	// Methods getters
	// ******************************************************************************

    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig();



    /**
     * Get string key (considered as handler id).
     *
     * @return string
     */
    public function getStrKey();
	
	
	
	
	
	// Methods setters
	// ******************************************************************************

    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     */
    public function setConfig(array $tabConfig);





    // Methods execute
    // ******************************************************************************

    /**
     * Execute action,
     * from specified error.
     *
     * @param mixed $error
     * @return null|mixed
     */
    public function execute($error);
}