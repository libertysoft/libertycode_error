<?php
/**
 * Description :
 * This class allows to describe behavior of handler collection class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\handler\api;

use liberty_code\error\handler\api\HandlerInterface;



interface HandlerCollectionInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods value
	// ******************************************************************************

	/**
     * Check if specified handler key is found.
	 * 
	 * @param string $strKey
	 * @return boolean
	 */
	public function checkExists($strKey);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

	/**
	 * Get index array of keys.
     *
	 * @return array
	 */
	public function getTabKey();



	/**
	 * Get handler,
     * from specified key.
	 * 
	 * @param string $strKey
	 * @return null|HandlerInterface
	 */
	public function getObjHandler($strKey);



    /**
     * Get associative array of handlers,
     * from specified error.
     *
     * Return array format: handler key => handler
     *
     * @param mixed $error
     * @return HandlerInterface[]
     */
    public function getTabHandler($error);
	




	// Methods setters
	// ******************************************************************************

	/**
	 * Set handler and return its key.
	 * 
	 * @param HandlerInterface $objHandler
	 * @return string
     */
	public function setHandler(HandlerInterface $objHandler);



    /**
     * Set list of handlers (index array or collection) and
     * return its list of keys (index array).
     *
     * @param array|HandlerCollectionInterface $tabHandler
     * @return array
     */
    public function setTabHandler($tabHandler);



    /**
     * Remove handler and return its instance.
     *
     * @param string $strKey
     * @return HandlerInterface
     */
    public function removeHandler($strKey);



    /**
     * Remove all handlers.
     */
    public function removeHandlerAll();
}