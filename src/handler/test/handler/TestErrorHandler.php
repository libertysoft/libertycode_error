<?php

namespace liberty_code\error\handler\test\handler;

use liberty_code\error\handler\error\fix\model\FixThrowErrorHandler;

use liberty_code\error\handler\error\library\ConstErrorHandler;



class TestErrorHandler extends FixThrowErrorHandler
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstErrorHandler::TAB_CONFIG_KEY_ERROR_TYPE_INCLUDE => [
                E_ERROR,
                E_CORE_ERROR,
                E_COMPILE_ERROR,
                E_USER_ERROR,
                E_RECOVERABLE_ERROR
            ]
        );
    }



}