<?php

namespace liberty_code\error\handler\test\handler;

use liberty_code\error\handler\throwable\fix\model\FixThrowableHandler;

use Throwable;
use Exception;
use liberty_code\error\handler\throwable\library\ConstThrowableHandler;
use liberty_code\error\handler\test\TestException;



class TestExceptionHandler extends FixThrowableHandler
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstThrowableHandler::TAB_CONFIG_KEY_THROW_CLASS_PATH_INCLUDE => [
                TestException::class
            ]
        );
    }





    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function execute($error)
    {
        /** @var Throwable $error */
        echo('<br />Test exception (executed by ' . $this->getStrKey() . '): <pre>');
        var_dump(
            array(
                'class' => get_class($error),
                'Code' => $error->getCode(),
                'Message' => $error->getMessage()
            )
        );
        echo('</pre>');

        throw new Exception(
            sprintf(
                'Exception redirect: (%1$s) %2$s',
                strval($error->getCode()),
                $error->getMessage()
            ),
            300
        );
    }



}