<?php

namespace liberty_code\error\handler\test\handler;

use liberty_code\error\handler\throwable\fix\model\FixThrowableHandler;

use ErrorException;
use liberty_code\error\handler\throwable\library\ConstThrowableHandler;



class TestErrorThrowableHandler extends FixThrowableHandler
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstThrowableHandler::TAB_CONFIG_KEY_THROW_CLASS_PATH_INCLUDE => [
                ErrorException::class
            ]
        );
    }





    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function execute($error)
    {
        /** @var ErrorException $error */
        echo('<br />Error exception (executed by ' . $this->getStrKey() . '): <pre>');
        var_dump(
            array(
                'class' => get_class($error),
                'Level' => $error->getSeverity(),
                'Code' => $error->getCode(),
                'Message' => $error->getMessage()
            )
        );
        echo('</pre>');

        return sprintf('error-throwable:%1$s', get_class($error));
    }



}