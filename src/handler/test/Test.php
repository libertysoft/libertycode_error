<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/handler/test/handler/TestErrorHandler.php');
require_once($strRootAppPath . '/src/handler/test/handler/TestErrorThrowableHandler.php');
require_once($strRootAppPath . '/src/handler/test/handler/TestThrowableHandler.php');
require_once($strRootAppPath . '/src/handler/test/handler/TestThrowableHandler2.php');

// Use
use liberty_code\error\error\library\ToolBoxErrorExceptionFactory;
use liberty_code\error\handler\api\HandlerInterface;
use liberty_code\error\handler\model\DefaultHandlerCollection;

// Use test
use liberty_code\error\handler\test\handler\TestErrorHandler;
use liberty_code\error\handler\test\handler\TestErrorThrowableHandler;
use liberty_code\error\handler\test\handler\TestThrowableHandler;
use liberty_code\error\handler\test\handler\TestThrowableHandler2;



// Init var
$objTestErrorHandler = new TestErrorHandler();
$objTestErrorThrowableHandler = new TestErrorThrowableHandler();
$objTestThrowableHandler = new TestThrowableHandler();
$objTestThrowableHandler2 = new TestThrowableHandler2();
$tabHandler = array(
    $objTestErrorHandler,
    $objTestErrorThrowableHandler,
    $objTestThrowableHandler,
    $objTestThrowableHandler2
);

$objHandlerCollection = new DefaultHandlerCollection();
$objHandlerCollection->setTabHandler($tabHandler);



// Test check/get from key
$tabKey = $objHandlerCollection->getTabKey();
foreach($tabKey as $strKey)
{
    echo('Test check, get key "'.$strKey.'": <br />');
    try{
        $objHandler = $objHandlerCollection->getObjHandler($strKey);
        $boolHandlerExists = $objHandlerCollection->checkExists($strKey);

        echo('Check: <pre>');var_dump($boolHandlerExists);echo('</pre>');

        if(!is_null($objHandler))
        {
            echo('Get: class: <pre>');print_r(get_class($objHandler));echo('</pre>');
            echo('Get: config: <pre>');print_r($objHandler->getTabConfig());echo('</pre>');
            echo('Get: key: <pre>');var_dump($objHandler->getStrKey());echo('</pre>');
        }
        else
        {
            echo('Get: not found: <br />');
        }
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test handle error
$tabError = array(
    ToolBoxErrorExceptionFactory::getTabConfig(
        E_WARNING,
        'Warning test',
        null,
        __FILE__,
        __LINE__
    ), // Ko
    ToolBoxErrorExceptionFactory::getTabConfig(
        E_ERROR,
        'Error test',
        null,
        __FILE__,
        __LINE__
    ), // Ok: handle by error handler
    ToolBoxErrorExceptionFactory::getTabConfig(
        E_USER_NOTICE,
        'User notice test'
    ), // Ko
    ToolBoxErrorExceptionFactory::getTabConfig(
        E_USER_ERROR,
        'User error test'
    ), // Ok: handle by error handler
    new ErrorException(
        'ErrorException 1 test',
        400,
        E_USER_ERROR
    ), // Ok: handle by error throwable handler
    new Exception(
        'Exception 1 test',
        400
    ), // Ok: handle by throwable handlers
    new ErrorException(
        'ErrorException 2 test',
        500,
        E_ERROR
    ), // Ok: handle by error throwable handler
    new Exception(
        'Exception 2 test',
        500
    ) // Ok: handle by throwable handlers
);

foreach($tabError as $error)
{
    echo('Test error : <pre>');var_dump($error);echo('</pre>');

    try{
        $tabHandler = $objHandlerCollection->getTabHandler($error);
        echo('Count handler: <pre>');var_dump(count($tabHandler));echo('</pre>');

        foreach($tabHandler as $objHandler)
        {
            /**  @var HandlerInterface $objHandler */
            $objHandler->execute($error);
        }
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


