<?php
/**
 * Description :
 * This class allows to define default handler collection class.
 * key: handler key => handler.
 *
 * Default handler collection uses the following specified configuration:
 * [
 *     multi_catch_require(optional: got true if not found): true / false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\handler\model;

use liberty_code\library\bean\model\DefaultBean;
use liberty_code\error\handler\api\HandlerCollectionInterface;

use liberty_code\library\bean\library\ConstBean;
use liberty_code\error\handler\library\ConstHandler;
use liberty_code\error\handler\api\HandlerInterface;
use liberty_code\error\handler\exception\CollectionConfigInvalidFormatException;
use liberty_code\error\handler\exception\CollectionKeyInvalidFormatException;
use liberty_code\error\handler\exception\CollectionValueInvalidFormatException;



class DefaultHandlerCollection extends DefaultBean implements HandlerCollectionInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /** @var array */
    protected $tabConfig;




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     */
    public function __construct(
        array $tabConfig = null,
        array $tabData = array()
    )
    {
        // Init var
        $this->tabConfig = array();

        // Call parent constructor
        parent::__construct($tabData);

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            // Check value argument
            CollectionValueInvalidFormatException::setCheck($value);

            // Check key argument
            /** @var HandlerInterface $value */
            if(
                (!is_string($key)) ||
                ($key != $value->getStrKey())
            )
            {
                throw new CollectionKeyInvalidFormatException($key);
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }
	
	
	
	
	
	// Methods check
	// ******************************************************************************

    /**
     * Check multi catch required.
     *
     * @return boolean
     */
    protected function checkMultiCatchRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstHandler::TAB_COLLECTION_CONFIG_KEY_MULTI_CATCH_REQUIRE])) ||
            (intval($tabConfig[ConstHandler::TAB_COLLECTION_CONFIG_KEY_MULTI_CATCH_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function checkExists($strKey)
    {
        // Return result
        return (!is_null($this->getObjHandler($strKey)));
    }
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig()
    {
        // Return result
        return $this->tabConfig;
    }



    /**
     * @inheritdoc
     */
    public function getTabKey()
    {
        // Return result
        return $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY);
    }



    /**
     * @inheritdoc
     */
    public function getObjHandler($strKey)
    {
        // Init var
        $result = null;

        // Try to get handler object, if found
        try
        {
            if($this->beanDataExists($strKey))
            {
                $result = $this->beanGetData($strKey);
            }
        }
        catch(\Exception $e)
        {
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabHandler($error)
    {
        // Init var
        $result = array();
        $boolMultiCatch = $this->checkMultiCatchRequired();

        // Run all handlers
        /** @var HandlerInterface[] $tabHandler */
        $tabHandler = $this->__beanTabData; // Perf requirement: Get directly array of handlers
        foreach($tabHandler as $strKey => $objHandler)
        {
            // Get handler, required
            if($objHandler->checkCatches($error))
            {
                // Set handler on result
                $result[$strKey] = $objHandler;

                // Break, if required
                if(!$boolMultiCatch) break;
            }
        }

        // Return result
        return $result;
    }



	
	
	// Methods setters
	// ******************************************************************************

    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     * @throws CollectionConfigInvalidFormatException
     */
    public function setConfig(array $tabConfig)
    {
        // Set check argument
        CollectionConfigInvalidFormatException::setCheck($tabConfig);

        $this->tabConfig = $tabConfig;
    }



	/**
	 * @inheritdoc
	 * @throws CollectionKeyInvalidFormatException
	 * @throws CollectionValueInvalidFormatException
     */
	public function setHandler(HandlerInterface $objHandler)
	{
		// Init var
		$strKey = $objHandler->getStrKey();
		
		// Register instance
		$this->beanPutData($strKey, $objHandler);
		
		// return result
		return $strKey;
	}



    /**
     * @inheritdoc
     */
    public function setTabHandler($tabHandler)
    {
        // Init var
        $result = array();

        // Case index array of handlers
        if(is_array($tabHandler))
        {
            // Run all handlers and for each, try to set
            foreach($tabHandler as $handler)
            {
                $strKey = $this->setHandler($handler);
                $result[] = $strKey;
            }
        }
        // Case collection of handlers
        else if($tabHandler instanceof HandlerCollectionInterface)
        {
            // Run all handlers and for each, try to set
            foreach($tabHandler->getTabKey() as $strKey)
            {
                $objHandler = $tabHandler->getObjHandler($strKey);
                $strKey = $this->setHandler($objHandler);
                $result[] = $strKey;
            }
        }

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeHandler($strKey)
    {
        // Init var
        $result = $this->getObjHandler($strKey);

        // Remove route
        $this->beanRemoveData($strKey);

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeHandlerAll()
    {
        // Ini var
        $tabKey = $this->getTabKey();

        foreach($tabKey as $strKey)
        {
            $this->removeHandler($strKey);
        }
    }



}