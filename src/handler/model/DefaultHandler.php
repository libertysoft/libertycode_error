<?php
/**
 * Description :
 * This class allows to define default handler class.
 * Can be consider is base of all handler type.
 *
 * Default handler uses the following specified configuration:
 * [
 *     key(optional: got static class name if not found): "string handler key"
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\handler\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\error\handler\api\HandlerInterface;

use liberty_code\library\reflection\library\ToolBoxClassReflection;
use liberty_code\error\handler\library\ConstHandler;
use liberty_code\error\handler\exception\ConfigInvalidFormatException;



abstract class DefaultHandler extends FixBean implements HandlerInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     */
    public function __construct(array $tabConfig = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstHandler::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstHandler::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstHandler::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstHandler::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     * Overwrite it to set specific feature.
     */
    public function checkCatches($error)
    {
        // Return result
        return true;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Return result
        return $this->beanGet(ConstHandler::DATA_KEY_DEFAULT_CONFIG);
    }
	
	

    /**
     * @inheritdoc
     */
    public function getStrKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstHandler::TAB_CONFIG_KEY_KEY, $tabConfig) ?
                $tabConfig[ConstHandler::TAB_CONFIG_KEY_KEY] :
                ToolBoxClassReflection::getStrClassName(static::class)
        );

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setConfig(array $tabConfig)
    {
        $this->beanSet(ConstHandler::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }



}