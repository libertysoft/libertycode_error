<?php
/**
 * Description :
 * This class allows to define standard handler factory class.
 * Standard handler factory allows to provide and hydrate handler instance.
 *
 * Standard handler factory uses the following specified configuration, to get and hydrate handler:
 * [
 *     -> Configuration key(optional): "handler key"
 *     type(required): "throw_error",
 *     Throw error handler configuration (@see ThrowErrorHandler )
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\handler\factory\standard\model;

use liberty_code\error\handler\factory\model\DefaultHandlerFactory;

use liberty_code\error\handler\error\model\ThrowErrorHandler;
use liberty_code\error\handler\factory\standard\library\ConstStandardHandlerFactory;



class StandardHandlerFactory extends DefaultHandlerFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrHandlerClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of handler, from type
        switch($strConfigType)
        {
            case null:
            case ConstStandardHandlerFactory::CONFIG_TYPE_THROW_ERROR:
                $result = ThrowErrorHandler::class;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getObjHandlerNew($strClassPath)
    {
        // Init var
        $result = null;

        // Get handler instance, from class
        if(is_string($strClassPath))
        {
            switch($strClassPath)
            {
                case ThrowErrorHandler::class:
                    $result = new ThrowErrorHandler();
                    break;
            }
        }

        // Return result
        return $result;
    }



}