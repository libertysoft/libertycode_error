<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/handler/factory/test/HandlerFactoryTest.php');
require_once($strRootAppPath . '/src/handler/test/handler/TestErrorHandler.php');
require_once($strRootAppPath . '/src/handler/test/handler/TestErrorThrowableHandler.php');
require_once($strRootAppPath . '/src/handler/test/handler/TestThrowableHandler.php');
require_once($strRootAppPath . '/src/handler/test/handler/TestThrowableHandler2.php');

// Use
use liberty_code\error\handler\error\model\ThrowErrorHandler;

// Use test
use liberty_code\error\handler\test\handler\TestErrorHandler;
use liberty_code\error\handler\test\handler\TestErrorThrowableHandler;
use liberty_code\error\handler\test\handler\TestThrowableHandler;
use liberty_code\error\handler\test\handler\TestThrowableHandler2;



// Init var
$objTestErrorHandler = new TestErrorHandler();
$objTestErrorThrowableHandler = new TestErrorThrowableHandler();
$objTestThrowableHandler = new TestThrowableHandler();
$objTestThrowableHandler2 = new TestThrowableHandler2();



// Test new handler
$tabHandlerData = array(
    [
        array_merge(
            $objTestErrorHandler->getTabConfig(),
            [
                'file_path' => $strRootAppPath . '/src/handler/test/handler/TestErrorHandler.php',
                'key' => 'error_handler'
            ]
        ),
        'error_handler_not_care',
        $objTestErrorHandler
    ], // Ok

    [
        [
            'type' => 'throw_error'
        ],
        'throw_error_handler',
        $objTestErrorHandler
    ], // Ko: not found: error handler used for throw error handler config

    [
        [
            'type' => 'throw_error'
        ],
        'throw_error_handler',
        new ThrowErrorHandler()
    ], // Ok

    [
        array_merge(
            $objTestThrowableHandler->getTabConfig(),
            [
                'file_path' => $strRootAppPath . '/src/handler/test/handler/TestThrowableHandler.php',
                'key' => 'throwable_handler'
            ]
        )
    ], // Ok

    [
        [
            'file_path' => $strRootAppPath . '/src/handler/test/handler/TestThrowableHandler2.php'
        ]
    ] // Ok
);

foreach($tabHandlerData as $handlerData)
{
    echo('Test new handler: <br />');
    echo('<pre>');var_dump($handlerData);echo('</pre>');

    try{
        $tabConfig = $handlerData[0];
        $strConfigKey = (isset($handlerData[1]) ? $handlerData[1] : null);
        $objHandler = (isset($handlerData[2]) ? $handlerData[2] : null);
        $objHandler = $objHandlerFactory->getObjHandler($tabConfig, $strConfigKey, $objHandler);

        echo('Class path: <pre>');var_dump($objHandlerFactory->getStrHandlerClassPath($tabConfig, $strConfigKey));echo('</pre>');

        if(!is_null($objHandler))
        {
            echo('Handler class path: <pre>');var_dump(get_class($objHandler));echo('</pre>');
            echo('Handler config: <pre>');var_dump($objHandler->getTabConfig());echo('</pre>');
            echo('Handler key: <pre>');var_dump($objHandler->getStrKey());echo('</pre>');
        }
        else
        {
            echo('Handler not found<br />');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ':' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test new handler from file
$tabHandlerData = array(
    [
        $strRootAppPath . '/src/handler/test/handler/TestErrorHandler_not_care.php',
        array_merge(
            $objTestErrorHandler->getTabConfig(),
            [
                'file_path' => $strRootAppPath . '/src/handler/test/handler/TestErrorHandler.php',
                'key' => 'error_handler'
            ]
        ),
        $objTestErrorThrowableHandler
    ], // Ko: not found: error throwable handler used for error handler config

    [
        $strRootAppPath . '/src/handler/test/handler/TestErrorHandler_not_care.php',
        array_merge(
            $objTestErrorHandler->getTabConfig(),
            [
                'file_path' => $strRootAppPath . '/src/handler/test/handler/TestErrorHandler.php',
                'key' => 'error_handler'
            ]
        ),
        $objTestErrorHandler
    ], // Ok

    [
        $strRootAppPath . '/src/handler/test/handler/TestErrorThrowableHandler.php',
        array_merge(
            $objTestErrorThrowableHandler->getTabConfig(),
            [
                'key' => 'error_throwable_handler'
            ]
        ),
        $objTestErrorThrowableHandler
    ], // Ok

    [
        null,
        [
            'file_path' => $strRootAppPath . '/src/handler/test/handler/TestThrowableHandler.php',
        ]
    ], // Ok

    [
        $strRootAppPath . '/src/handler/test/handler/TestThrowableHandler2.php'
    ] // Ok
);

foreach($tabHandlerData as $handlerData)
{
    echo('Test new handler from file: <br />');
    echo('<pre>');var_dump($handlerData);echo('</pre>');

    try{
        $strFilePath = $handlerData[0];
        $tabConfig = (isset($handlerData[1]) ? $handlerData[1] : null);
        $objHandler = (isset($handlerData[2]) ? $handlerData[2] : null);
        $objHandler = $objHandlerFactory->getObjHandlerFromFile($strFilePath, $tabConfig, $objHandler);

        echo('Class path: <pre>');var_dump($objHandlerFactory->getStrHandlerClassPathFromFile($strFilePath, $tabConfig));echo('</pre>');

        if(!is_null($objHandler))
        {
            echo('Handler class path: <pre>');var_dump(get_class($objHandler));echo('</pre>');
            echo('Handler config: <pre>');var_dump($objHandler->getTabConfig());echo('</pre>');
            echo('Handler key: <pre>');var_dump($objHandler->getStrKey());echo('</pre>');
        }
        else
        {
            echo('Handler not found<br />');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ':' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


