<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\di\dependency\model\DefaultDependencyCollection;
use liberty_code\di\provider\model\DefaultProvider;
use liberty_code\error\handler\factory\model\DefaultHandlerFactory;
use liberty_code\error\handler\factory\standard\model\StandardHandlerFactory;



// Init DI
$objRegister = new MemoryRegister();
$objDepCollection = new DefaultDependencyCollection($objRegister);
$objProvider = new DefaultProvider($objDepCollection);



// Init var
$objHandlerFactory = new DefaultHandlerFactory(null, $objProvider);
$objHandlerFactory = new StandardHandlerFactory($objHandlerFactory, $objProvider);


