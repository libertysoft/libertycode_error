<?php
/**
 * Description :
 * This class allows to describe behavior of handler factory class.
 * Handler factory allows to provide new or specified handler instance,
 * hydrated with a specified configuration,
 * from a set of potential predefined handler types or file path.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\handler\factory\api;

use liberty_code\error\handler\api\HandlerInterface;



interface HandlerFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string class path of handler,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return null|string
     */
    public function getStrHandlerClassPath(array $tabConfig, $strConfigKey = null);



    /**
     * Get string class path of handler,
     * from specified file path
     * and from specified configuration.
     *
     * @param string $strFilePath
     * @param array $tabConfig = null
     * @return null|string
     */
    public function getStrHandlerClassPathFromFile($strFilePath, array $tabConfig = null);



    /**
     * Get new or specified object instance handler,
     * hydrated from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param HandlerInterface $objHandler = null
     * @return null|HandlerInterface
     */
    public function getObjHandler(
        array $tabConfig,
        $strConfigKey = null,
        HandlerInterface $objHandler = null
    );



    /**
     * Get new or specified object instance handler,
     * hydrated from specified file path,
     * and from specified configuration.
     *
     * @param string $strFilePath
     * @param array $tabConfig = null
     * @param HandlerInterface $objHandler = null
     * @return null|HandlerInterface
     */
    public function getObjHandlerFromFile(
        $strFilePath,
        array $tabConfig = null,
        HandlerInterface $objHandler = null
    );
}