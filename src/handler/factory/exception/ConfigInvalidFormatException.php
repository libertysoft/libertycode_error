<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\handler\factory\exception;

use liberty_code\error\handler\factory\library\ConstHandlerFactory;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstHandlerFactory::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid type
            (
                (!isset($config[ConstHandlerFactory::TAB_CONFIG_KEY_TYPE])) ||
                (
                    is_string($config[ConstHandlerFactory::TAB_CONFIG_KEY_TYPE]) &&
                    (trim($config[ConstHandlerFactory::TAB_CONFIG_KEY_TYPE]) != '')
                )
            ) &&

            // Check valid file path
            (
                (!isset($config[ConstHandlerFactory::TAB_CONFIG_KEY_FILE_PATH])) ||
                (
                    is_string($config[ConstHandlerFactory::TAB_CONFIG_KEY_FILE_PATH]) &&
                    (trim($config[ConstHandlerFactory::TAB_CONFIG_KEY_FILE_PATH]) != '') &&
                    file_exists($config[ConstHandlerFactory::TAB_CONFIG_KEY_FILE_PATH]) &&
                    is_file($config[ConstHandlerFactory::TAB_CONFIG_KEY_FILE_PATH]) &&
                    is_readable($config[ConstHandlerFactory::TAB_CONFIG_KEY_FILE_PATH]) &&
                    (trim(strtolower(pathinfo(
                        $config[ConstHandlerFactory::TAB_CONFIG_KEY_FILE_PATH]
                        , PATHINFO_EXTENSION
                    ))) == 'php')
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}