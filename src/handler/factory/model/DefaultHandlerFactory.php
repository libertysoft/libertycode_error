<?php
/**
 * Description :
 * This class allows to define default handler factory class.
 * Can be consider is base of all handler factory type.
 *
 * Default handler factory uses the following specified configuration, to get and hydrate handler:
 * [
 *     file_path(optional): "file path to handler class",
 *
 *     OR
 *
 *     type(optional): "string constant to determine handler type",
 *
 *     ... specific handler configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\handler\factory\model;

use liberty_code\di\factory\model\DefaultFactory;
use liberty_code\error\handler\factory\api\HandlerFactoryInterface;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\library\reflection\library\ToolBoxFileReflection;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\error\handler\library\ConstHandler;
use liberty_code\error\handler\api\HandlerInterface;
use liberty_code\error\handler\factory\library\ConstHandlerFactory;
use liberty_code\error\handler\factory\exception\FactoryInvalidFormatException;
use liberty_code\error\handler\factory\exception\ConfigInvalidFormatException;



/**
 * @method null|HandlerFactoryInterface getObjFactory() Get parent factory object.
 * @method void setObjFactory(null|HandlerFactoryInterface $objFactory) Set parent factory object.
 */
class DefaultHandlerFactory extends DefaultFactory implements HandlerFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param HandlerFactoryInterface $objFactory = null
     */
    public function __construct(
        HandlerFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init handler factory
        $this->setObjFactory($objFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstHandlerFactory::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstHandlerFactory::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * Hydrate specified handler.
     * Overwrite it to set specific hydration.
     *
     * @param HandlerInterface $objHandler
     * @param array $tabConfigFormat
     */
    protected function hydrateHandler(HandlerInterface $objHandler, array $tabConfigFormat)
    {
        // Init formatted configuration
        if(array_key_exists(ConstHandlerFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            unset($tabConfigFormat[ConstHandlerFactory::TAB_CONFIG_KEY_TYPE]);
        }
        if(array_key_exists(ConstHandlerFactory::TAB_CONFIG_KEY_FILE_PATH, $tabConfigFormat))
        {
            unset($tabConfigFormat[ConstHandlerFactory::TAB_CONFIG_KEY_FILE_PATH]);
        }

        // Hydrate handler
        if(count($tabConfigFormat) > 0)
        {
            $objHandler->setConfig($tabConfigFormat);
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstHandlerFactory::DATA_KEY_DEFAULT_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstHandlerFactory::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified formatted configuration is valid,
     * for the specified handler object.
     *
     * @param HandlerInterface $objHandler
     * @param array $tabConfigFormat
     * @return boolean
     * @throws ConfigInvalidFormatException
     */
    protected function checkConfigIsValid(HandlerInterface $objHandler, array $tabConfigFormat)
    {
        // Init var
        $strHandlerClassPath = $this->getStrHandlerClassPathEngine($tabConfigFormat);
        $result = (
            (!is_null($strHandlerClassPath)) &&
            ($strHandlerClassPath == get_class($objHandler))
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get formatted configuration array.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param string $strFilePath = null
     * @return array
     */
    protected function getTabConfigFormat(
        array $tabConfig,
        $strConfigKey = null,
        $strFilePath = null
    )
    {
        // Init var
        $result = $tabConfig;

        // Use configuration key, considered as handler key, if required
        if(
            (!is_null($strConfigKey)) &&
            (!array_key_exists(ConstHandler::TAB_CONFIG_KEY_KEY, $result))
        )
        {
            $result[ConstHandler::TAB_CONFIG_KEY_KEY] = $strConfigKey;
        }

        // Use file path, if required
        if(
            (!is_null($strFilePath)) &&
            (!array_key_exists(ConstHandlerFactory::TAB_CONFIG_KEY_FILE_PATH, $result))
        )
        {
            $result[ConstHandlerFactory::TAB_CONFIG_KEY_FILE_PATH] = $strFilePath;
        }

        // Return result
        return $result;
    }



    /**
     * Get string configured file path,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     */
    protected function getStrConfigFilePath(array $tabConfigFormat)
    {
        // Init var
        $result = null;

        // Get type, if found
        if(array_key_exists(ConstHandlerFactory::TAB_CONFIG_KEY_FILE_PATH, $tabConfigFormat))
        {
            $result = $tabConfigFormat[ConstHandlerFactory::TAB_CONFIG_KEY_FILE_PATH];
        }

        // Return result
        return $result;
    }



    /**
     * Get string configured type,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     */
    protected function getStrConfigType(array $tabConfigFormat)
    {
        // Init var
        $result = null;

        // Get type, if found
        if(array_key_exists(ConstHandlerFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            $result = $tabConfigFormat[ConstHandlerFactory::TAB_CONFIG_KEY_TYPE];
        }

        // Return result
        return $result;
    }



    /**
     * Get string class path of handler,
     * from specified configured file path.
     *
     * @param null|string $strConfigFilePath
     * @return null|string
     */
    protected function getStrHandlerClassPathFromFilePath($strConfigFilePath)
    {
        // Init var
        $result = null;
        $tabClassPath = array_merge(
            ToolBoxFileReflection::getTabClassPath($strConfigFilePath),
            ToolBoxFileReflection::getTabInterfacePath($strConfigFilePath)
        );

        // Get class path, if required (first class path found)
        if(
            isset($tabClassPath[0]) &&
            is_subclass_of($tabClassPath[0], HandlerInterface::class)
        )
        {
            $result = $tabClassPath[0];
        }

        // Return result
        return $result;
    }



    /**
     * Get string class path of handler,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|string
     */
    protected function getStrHandlerClassPathFromType($strConfigType)
    {
        // Return result
        return null;
    }



    /**
     * Get string class path of handler engine,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrHandlerClassPathEngine(array $tabConfigFormat)
    {
        // Check arguments
        ConfigInvalidFormatException::setCheck($tabConfigFormat);

        // Init var
        $strConfigFilePath = $this->getStrConfigFilePath($tabConfigFormat);
        $strConfigType = $this->getStrConfigType($tabConfigFormat);

        // Get class path
        $result = $this->getStrHandlerClassPathFromFilePath($strConfigFilePath);
        if(is_null($result))
        {
            $result = $this->getStrHandlerClassPathFromType($strConfigType);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getStrHandlerClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getStrHandlerClassPathEngine($tabConfigFormat);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrHandlerClassPath($tabConfig, $strConfigKey);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getStrHandlerClassPathFromFile($strFilePath, array $tabConfig = null)
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat(
            (is_null($tabConfig) ? array() : $tabConfig),
            null,
            $strFilePath
        );
        $result = $this->getStrHandlerClassPathEngine($tabConfigFormat);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrHandlerClassPathFromFile($strFilePath, $tabConfig);
        }

        // Return result
        return $result;
    }



    /**
     * Get new object instance handler,
     * from specified class path.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strClassPath
     * @return null|HandlerInterface
     */
    protected function getObjHandlerNew($strClassPath)
    {
        // Init var
        $result = $this->getObjInstance($strClassPath);
        $result = (
            is_null($result) ?
                ToolBoxReflection::getObjInstance($strClassPath) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get object instance handler engine.
     *
     * @param array $tabConfigFormat
     * @param HandlerInterface $objHandler = null
     * @return null|HandlerInterface
     * @throws ConfigInvalidFormatException
     */
    protected function getObjHandlerEngine(
        array $tabConfigFormat,
        HandlerInterface $objHandler = null
    )
    {
        // Init var
        $result = null;
        $strClassPath = $this->getStrHandlerClassPathEngine($tabConfigFormat);
        $objHandler = (
            is_null($objHandler) ?
                $this->getObjHandlerNew($strClassPath) :
                $objHandler
        );

        // Get and hydrate handler, if required
        if(
            (!is_null($objHandler)) &&
            $this->checkConfigIsValid($objHandler, $tabConfigFormat)
        )
        {
            $this->hydrateHandler($objHandler, $tabConfigFormat);
            $result = $objHandler;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getObjHandler(
        array $tabConfig,
        $strConfigKey = null,
        HandlerInterface $objHandler = null
    )
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getObjHandlerEngine($tabConfigFormat, $objHandler);

        // Get handler from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjHandler($tabConfig, $strConfigKey, $objHandler);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getObjHandlerFromFile(
        $strFilePath,
        array $tabConfig = null,
        HandlerInterface $objHandler = null
    )
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat(
            (is_null($tabConfig) ? array() : $tabConfig),
            null,
            $strFilePath
        );
        $result = $this->getObjHandlerEngine($tabConfigFormat, $objHandler);

        // Get handler from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjHandlerFromFile($strFilePath, $tabConfig, $objHandler);
        }

        // Return result
        return $result;
    }



}