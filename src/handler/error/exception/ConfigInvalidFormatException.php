<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\handler\error\exception;

use liberty_code\error\handler\error\library\ConstErrorHandler;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstErrorHandler::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init error level index array check function
        $checkTabErrTypeIsValid = function(array $tabErrType)
        {
            $result = true;

            // Check each error level is valid
            for($intCpt = 0; ($intCpt < count($tabErrType)) && $result; $intCpt++)
            {
                $intErrTypeValue = $tabErrType[$intCpt];
                $result =
                    is_int($intErrTypeValue) &&
                    (($intErrTypeValue) > 0);
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid error level included
            (
                (!isset($config[ConstErrorHandler::TAB_CONFIG_KEY_ERROR_TYPE_INCLUDE])) ||
                (
                    is_array($config[ConstErrorHandler::TAB_CONFIG_KEY_ERROR_TYPE_INCLUDE]) &&
                    $checkTabErrTypeIsValid($config[ConstErrorHandler::TAB_CONFIG_KEY_ERROR_TYPE_INCLUDE])
                )
            ) &&

            // Check valid error level excluded
            (
                (!isset($config[ConstErrorHandler::TAB_CONFIG_KEY_ERROR_TYPE_EXCLUDE])) ||
                (
                    is_array($config[ConstErrorHandler::TAB_CONFIG_KEY_ERROR_TYPE_EXCLUDE]) &&
                    $checkTabErrTypeIsValid($config[ConstErrorHandler::TAB_CONFIG_KEY_ERROR_TYPE_EXCLUDE])
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}