<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\handler\error\library;



class ConstErrorHandler
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_ERROR_TYPE_INCLUDE = 'error_type_include';
    const TAB_CONFIG_KEY_ERROR_TYPE_EXCLUDE = 'error_type_exclude';
	
	
	
    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the error handler configuration standard.';
}