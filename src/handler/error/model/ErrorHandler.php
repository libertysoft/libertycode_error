<?php
/**
 * Description :
 * This class allows to define error handler class.
 * Error handler allows to handle standard errors,
 * with error exception.
 *
 * Error handler uses the following specified configuration:
 * [
 *     Default handler configuration,
 *
 *     error_type_include(optional): [
 *         Integer error level 1,
 *         ...,
 *         Integer error level N
 *     ],
 *
 *     error_type_exclude(optional): [
 *         Integer error level 1,
 *         ...,
 *         Integer error level N
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\handler\error\model;

use liberty_code\error\handler\model\DefaultHandler;

use ErrorException;
use liberty_code\error\error\library\ToolBoxErrorExceptionFactory;
use liberty_code\error\handler\library\ConstHandler;
use liberty_code\error\handler\error\library\ConstErrorHandler;
use liberty_code\error\handler\error\exception\ConfigInvalidFormatException;



abstract class ErrorHandler extends DefaultHandler
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstHandler::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkCatches($error)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $tabErrTypeInclude = (
            isset($tabConfig[ConstErrorHandler::TAB_CONFIG_KEY_ERROR_TYPE_INCLUDE]) ?
                $tabConfig[ConstErrorHandler::TAB_CONFIG_KEY_ERROR_TYPE_INCLUDE] :
                null
        );
        $tabErrTypeExclude = (
            isset($tabConfig[ConstErrorHandler::TAB_CONFIG_KEY_ERROR_TYPE_EXCLUDE]) ?
                $tabConfig[ConstErrorHandler::TAB_CONFIG_KEY_ERROR_TYPE_EXCLUDE] :
                null
        );
        $objErrorException = $this->getObjErrorException($error);
        $result = (
            (!is_null($objErrorException)) &&
            (
                is_null($tabErrTypeInclude) ||
                in_array($objErrorException->getSeverity(), $tabErrTypeInclude)
            ) &&
            (
                is_null($tabErrTypeExclude) ||
                (!in_array($objErrorException->getSeverity(), $tabErrTypeExclude))
            )
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get new error exception object,
     * from specified error.
     * Return null, if error failed.
     *
     * To success, error must be an array, following configuration array format:
     * @see ToolBoxErrorExceptionFactory::getObjErrorException() configuration array format.
     *
     * @param mixed $error
     * @return null|ErrorException
     */
    public static function getObjErrorException($error)
    {
        // Return result
        return (
            is_array($error) ?
                ToolBoxErrorExceptionFactory::getObjErrorException($error) :
                null
        );
    }



}