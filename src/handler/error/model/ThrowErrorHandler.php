<?php
/**
 * Description :
 * This class allows to define throw error handler class.
 * Throw error handler is error handler,
 * which throws error exception.
 *
 * Throw error handler uses the following specified configuration:
 * [
 *     Error handler configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\handler\error\model;

use liberty_code\error\handler\error\model\ErrorHandler;

use ErrorException;



class ThrowErrorHandler extends ErrorHandler
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ErrorException
     */
    public function execute($error)
    {
        // Init var
        $objErrorException = $this->getObjErrorException($error);

        // Throw error exception, if required
        if(!is_null($objErrorException))
        {
            throw $objErrorException;
        }
    }



}