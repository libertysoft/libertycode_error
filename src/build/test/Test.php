<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/build/test/HandlerBuilderTest.php');
require_once($strRootAppPath . '/src/handler/test/handler/TestErrorHandler.php');
require_once($strRootAppPath . '/src/handler/test/handler/TestErrorThrowableHandler.php');
require_once($strRootAppPath . '/src/handler/test/handler/TestThrowableHandler.php');
require_once($strRootAppPath . '/src/handler/test/handler/TestThrowableHandler2.php');

// Use
use liberty_code\error\handler\api\HandlerInterface;
use liberty_code\error\handler\model\DefaultHandlerCollection;

// Use test
use liberty_code\error\handler\test\handler\TestErrorHandler;
use liberty_code\error\handler\test\handler\TestErrorThrowableHandler;
use liberty_code\error\handler\test\handler\TestThrowableHandler;
use liberty_code\error\handler\test\handler\TestThrowableHandler2;



// Init var
$objTestErrorHandler = new TestErrorHandler();
$objTestErrorThrowableHandler = new TestErrorThrowableHandler();
$objTestThrowableHandler = new TestThrowableHandler();
$objTestThrowableHandler2 = new TestThrowableHandler2();
$objHandlerCollection = new DefaultHandlerCollection();

$tabDataSrc = array(
    'error_handler' =>  array_merge(
        $objTestErrorHandler->getTabConfig(),
        [
            'file_path' => $strRootAppPath . '/src/handler/test/handler/TestErrorHandler.php'
        ]
    ),
    /*
    'throw_error_handler' => array_merge(
        $objTestErrorThrowableHandler->getTabConfig(),
        [
            'file_path' => $strRootAppPath . '/src/handler/test/handler/TestErrorThrowableHandler.php'
        ]
    ),
    //*/
    [
        'type' => 'throw_error'
    ],
    [
        'file_path' => $strRootAppPath . '/src/handler/test/handler/TestThrowableHandler.php',
    ],
    array_merge(
        $objTestThrowableHandler2->getTabConfig(),
        [
            'file_path' => $strRootAppPath . '/src/handler/test/handler/TestThrowableHandler2.php',
            'key' => 'throwable_handler_2'
        ]
    )
);



// Test properties
echo('Test properties: <br />');
echo('Data source initialization: <pre>');print_r($objHandlerBuilder->getTabDataSrc());echo('</pre>');

echo('<br />');

$objHandlerBuilder->setTabDataSrc($tabDataSrc);
echo('Data source hydrated: <pre>');print_r($objHandlerBuilder->getTabDataSrc());echo('</pre>');

echo('<br />');

echo('<br /><br /><br />');



// Test hydrate handler collection
echo('Test hydrate handler collection: <br />');

$objHandlerBuilder->hydrateHandlerCollection($objHandlerCollection, false);
foreach($objHandlerCollection as $strKey => $objHandler)
{
    /** @var HandlerInterface $objHandler */
	try{
		echo('Handler "' . $strKey . '":');echo('<br />');
        echo('Get: class: <pre>');print_r(get_class($objHandler));echo('</pre>');
        echo('Get: config: <pre>');print_r($objHandler->getTabConfig());echo('</pre>');
        echo('Get: key: <pre>');var_dump($objHandler->getStrKey());echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	
	echo('<br /><br />');
}

echo('<br /><br /><br />');



// Test clear handler collection
echo('Test clear: <br />');

echo('Before clearing: <pre>');print_r($objHandlerCollection->getTabKey());echo('</pre>');

$objHandlerBuilder->setTabDataSrc(array());
$objHandlerBuilder->hydrateHandlerCollection($objHandlerCollection);
echo('After clearing: <pre>');print_r($objHandlerCollection->getTabKey());echo('</pre>');

echo('<br /><br /><br />');



// Test hydrate handler collection (with directory)
$tabDataSrc = array(
    $strRootAppPath . '/src/handler/test/handler'
);

echo('Test hydrate handler collection (with directory): <br />');

$objHandlerDirBuilder->setTabDataSrc($tabDataSrc);
$objHandlerDirBuilder->hydrateHandlerCollection($objHandlerCollection, false);
foreach($objHandlerCollection as $strKey => $objHandler)
{
    /** @var HandlerInterface $objHandler */
    try{
        echo('Handler "' . $strKey . '":');echo('<br />');
        echo('Get: class: <pre>');print_r(get_class($objHandler));echo('</pre>');
        echo('Get: config: <pre>');print_r($objHandler->getTabConfig());echo('</pre>');
        echo('Get: key: <pre>');var_dump($objHandler->getStrKey());echo('</pre>');
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }

    echo('<br /><br />');
}

echo('<br /><br /><br />');


