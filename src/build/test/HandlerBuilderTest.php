<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/handler/factory/test/HandlerFactoryTest.php');

// Use
use liberty_code\error\build\model\DefaultBuilder;
use liberty_code\error\build\directory\model\DirBuilder;

$objHandlerBuilder = new DefaultBuilder(
    $objHandlerFactory
);

$objHandlerDirBuilder = new DirBuilder(
    $objHandlerFactory
);


