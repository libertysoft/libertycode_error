<?php
/**
 * Description :
 * This class allows to describe behavior of builder class.
 * Builder allows to populate specified handler collection instance, with handlers.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\build\api;

use liberty_code\error\handler\api\HandlerCollectionInterface;



interface BuilderInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified handler collection.
     *
     * @param HandlerCollectionInterface $objHandlerCollection
     * @param boolean $boolClear = true
     */
    public function hydrateHandlerCollection(HandlerCollectionInterface $objHandlerCollection, $boolClear = true);
}