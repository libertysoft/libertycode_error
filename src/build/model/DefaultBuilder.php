<?php
/**
 * Description :
 * This class allows to define default builder class.
 * Default builder allows to populate handler collection,
 * from a specified array of source data.
 *
 * Default builder uses the following specified source data, to hydrate handler collection:
 * [
 *     // Handler configuration 1
 *     Handler factory configuration (@see HandlerFactoryInterface ),
 *
 *     ...,
 *
 *     // Handler configuration N
 *     ...
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\build\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\error\build\api\BuilderInterface;

use liberty_code\error\handler\api\HandlerCollectionInterface;
use liberty_code\error\handler\factory\api\HandlerFactoryInterface;
use liberty_code\error\build\library\ConstBuilder;
use liberty_code\error\build\exception\FactoryInvalidFormatException;
use liberty_code\error\build\exception\DataSrcInvalidFormatException;



/**
 * @method null|HandlerFactoryInterface getObjFactory() Get handler factory object.
 * @method void setObjFactory(null|HandlerFactoryInterface $objFactory) Set handler factory object.
 * @method array getTabDataSrc() get data source array.
 * @method void setTabDataSrc(array $tabDataSrc) Set data source array.
 */
class DefaultBuilder extends FixBean implements BuilderInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param HandlerFactoryInterface $objFactory
     * @param array $tabDataSrc = array()
     */
    public function __construct(
        HandlerFactoryInterface $objFactory,
        array $tabDataSrc = array()
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init handler factory
        $this->setObjFactory($objFactory);

        // Init data source
        $this->setTabDataSrc($tabDataSrc);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->beanAdd(ConstBuilder::DATA_KEY_DEFAULT_FACTORY, null);
        }

        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC))
        {
            $this->beanAdd(ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC, array());
        }
    }



    /**
     * @inheritdoc
     * @throws DataSrcInvalidFormatException
     */
    public function hydrateHandlerCollection(HandlerCollectionInterface $objHandlerCollection, $boolClear = true)
    {
        // Init var
        $boolClear = (is_bool($boolClear) ? $boolClear : true);
        $objFactory = $this->getObjFactory();
        $tabDataSrc = $this->getTabDataSrc();

        // Run each data source
        $tabHandler = array();
        foreach($tabDataSrc as $key => $tabConfig)
        {
            // Get new handler
            $key = (is_string($key) ? $key : null);
            $objHandler = $objFactory->getObjHandler($tabConfig, $key);

            // Register handler, if found
            if(!is_null($objHandler))
            {
                $tabHandler[] = $objHandler;
            }
            // Throw exception if handler not found, from data source
            else
            {
                throw new DataSrcInvalidFormatException(serialize($tabDataSrc));
            }
        }

        // Clear handlers from collection, if required
        if($boolClear)
        {
            $objHandlerCollection->removeHandlerAll();
        }

        // Register handlers on collection
        $objHandlerCollection->setTabHandler($tabHandler);
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
            ConstBuilder::DATA_KEY_DEFAULT_FACTORY,
			ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstBuilder::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

				case ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC:
                    DataSrcInvalidFormatException::setCheck($value);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}
	
	
	
}