<?php
/**
 * Description :
 * This class allows to define directory builder class.
 * Directory builder allows to populate handler collection,
 * from a specified array of source data.
 *
 * Directory builder uses the following specified source data, to hydrate handler collection:
 * [
 *     "String unique handler directory path 1:
 *      Each php file considered as handler class"
 *
 *     ...,
 *
 *     "String unique handler directory path N"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\build\directory\model;

use liberty_code\error\build\model\DefaultBuilder;

use liberty_code\error\handler\api\HandlerCollectionInterface;
use liberty_code\error\build\library\ConstBuilder;
use liberty_code\error\build\directory\exception\DataSrcInvalidFormatException;



class DirBuilder extends DefaultBuilder
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws DataSrcInvalidFormatException
     */
    public function hydrateHandlerCollection(HandlerCollectionInterface $objHandlerCollection, $boolClear = true)
    {
        // Init var
        $boolClear = (is_bool($boolClear) ? $boolClear : true);
        $objFactory = $this->getObjFactory();
        $tabDataSrc = $this->getTabDataSrc();

        // Run each data source
        $tabHandler = array();
        foreach($tabDataSrc as $strDirPath)
        {
            $tabFilePath = glob($strDirPath . '/*.php');

            // Run all files
            foreach($tabFilePath as $strFilePath)
            {
                // Check valid file path
                if(is_file($strFilePath) && is_readable($strFilePath))
                {
                    // Get new handler
                    $objHandler = $objFactory->getObjHandlerFromFile($strFilePath);

                    // Register handler, if found
                    if(!is_null($objHandler))
                    {
                        $tabHandler[] = $objHandler;
                    }
                    // Throw exception if handler not found, from data source
                    else
                    {
                        throw new DataSrcInvalidFormatException(serialize($tabDataSrc));
                    }
                }
            }
        }

        // Clear handlers from collection, if required
        if($boolClear)
        {
            $objHandlerCollection->removeHandlerAll();
        }

        // Register handlers on collection
        $objHandlerCollection->setTabHandler($tabHandler);
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC:
                    DataSrcInvalidFormatException::setCheck($value);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}
	
	
	
}