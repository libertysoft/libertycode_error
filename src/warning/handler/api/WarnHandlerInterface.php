<?php
/**
 * Description :
 * This class allows to describe behavior of warning handler class.
 * Warning handler contains all information to handle specific warnings,
 * to execute specific action,
 * if main process succeeded or failed (error happened).
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\handler\api;



interface WarnHandlerInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig();



    /**
     * Get string key (considered as warning handler id).
     *
     * @return string
     */
    public function getStrKey();



    /**
     * Get index array of catch warnings,
     * from specified index array of warnings.
     *
     * @param mixed[] $tabWarning
     * @return mixed[]
     */
    public function getTabCatchWarning(array $tabWarning);
	
	
	
	
	
	// Methods setters
	// ******************************************************************************

    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     */
    public function setConfig(array $tabConfig);





    // Methods execute
    // ******************************************************************************

    /**
     * Execute action,
     * from specified index array of warnings.
     *
     * @param mixed[] array $tabWarning
     * @param null|mixed $resultExecution : main execution process return.
     * @param boolean $boolExecutionSuccess : true if main execution process success, false else (failure: error happened).
     * @return null|mixed
     */
    public function execute(array $tabWarning, $resultExecution, $boolExecutionSuccess);
}