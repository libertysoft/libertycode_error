<?php
/**
 * Description :
 * This class allows to describe behavior of warning handler collection class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\handler\api;

use liberty_code\error\warning\handler\api\WarnHandlerInterface;



interface WarnHandlerCollectionInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods value
	// ******************************************************************************

	/**
     * Check if specified warning handler key is found.
	 * 
	 * @param string $strKey
	 * @return boolean
	 */
	public function checkExists($strKey);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

	/**
	 * Get index array of keys.
     *
	 * @return array
	 */
	public function getTabKey();



	/**
	 * Get warning handler,
     * from specified key.
	 * 
	 * @param string $strKey
	 * @return null|WarnHandlerInterface
	 */
	public function getObjWarnHandler($strKey);



    /**
     * Get associative array of warning handlers,
     * from specified index array of warnings.
     *
     * Return array format: warning handler key => warning handler
     *
     * Array of warnings, per warning handlers, format:
     * [
     *     'warning handler key 1' => [index array of warnings],
     *     ...,
     *     'warning handler key N' => ...
     * ]
     *
     * @param mixed[] $tabWarning
     * @param array &$tabWarnHandlerWarning = array()
     * @return WarnHandlerInterface[]
     */
    public function getTabWarnHandler(array $tabWarning, array &$tabWarnHandlerWarning = array());
	




	// Methods setters
	// ******************************************************************************

	/**
	 * Set warning handler and return its key.
	 * 
	 * @param WarnHandlerInterface $objWarnHandler
	 * @return string
     */
	public function setWarnHandler(WarnHandlerInterface $objWarnHandler);



    /**
     * Set list of warning handlers (index array or collection) and
     * return its list of keys (index array).
     *
     * @param array|WarnHandlerCollectionInterface $tabWarnHandler
     * @return array
     */
    public function setTabWarnHandler($tabWarnHandler);



    /**
     * Remove warning handler and return its instance.
     *
     * @param string $strKey
     * @return WarnHandlerInterface
     */
    public function removeWarnHandler($strKey);



    /**
     * Remove all warning handlers.
     */
    public function removeWarnHandlerAll();
}