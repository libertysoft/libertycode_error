<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\handler\exception;

use liberty_code\error\warning\handler\library\ConstWarnHandler;
use liberty_code\error\warning\handler\api\WarnHandlerInterface;



class CollectionValueInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $value
     */
	public function __construct($value) 
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstWarnHandler::EXCEPT_MSG_COLLECTION_VALUE_INVALID_FORMAT, strval($value));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified value has valid format.
	 * 
     * @param mixed $value
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($value)
    {
		// Init var
		$result = (
		    (!is_null($value)) &&
            ($value instanceof WarnHandlerInterface)
        );
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($value);
		}
		
		// Return result
		return $result;
    }
	
	
	
}