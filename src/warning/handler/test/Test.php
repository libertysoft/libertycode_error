<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/warning/handler/test/handler/TestErrorThrowableWarnHandler.php');
require_once($strRootAppPath . '/src/warning/handler/test/handler/TestThrowableWarnHandler.php');
require_once($strRootAppPath . '/src/warning/handler/test/handler/TestThrowableWarnHandler2.php');

// Use
use liberty_code\error\error\library\ConstError;
use liberty_code\error\error\library\ToolBoxErrorExceptionFactory;
use liberty_code\error\warning\handler\api\WarnHandlerInterface;
use liberty_code\error\warning\handler\model\DefaultWarnHandlerCollection;

// Use test
use liberty_code\error\warning\handler\test\handler\TestErrorThrowableWarnHandler;
use liberty_code\error\warning\handler\test\handler\TestThrowableWarnHandler;
use liberty_code\error\warning\handler\test\handler\TestThrowableWarnHandler2;



// Init var
$objTestErrorThrowableWarnHandler = new TestErrorThrowableWarnHandler();
$objTestThrowableWarnHandler = new TestThrowableWarnHandler();
$objTestThrowableWarnHandler2 = new TestThrowableWarnHandler2();
$tabWarnHandler = array(
    $objTestErrorThrowableWarnHandler,
    $objTestThrowableWarnHandler,
    $objTestThrowableWarnHandler2
);

$objWarnHandlerCollection = new DefaultWarnHandlerCollection();
$objWarnHandlerCollection->setTabWarnHandler($tabWarnHandler);



// Test check/get from key
$tabKey = $objWarnHandlerCollection->getTabKey();
foreach($tabKey as $strKey)
{
    echo('Test check, get key "'.$strKey.'": <br />');
    try{
        $objWarnHandler = $objWarnHandlerCollection->getObjWarnHandler($strKey);
        $boolWarnHandlerExists = $objWarnHandlerCollection->checkExists($strKey);

        echo('Check: <pre>');var_dump($boolWarnHandlerExists);echo('</pre>');

        if(!is_null($objWarnHandler))
        {
            echo('Get: class: <pre>');print_r(get_class($objWarnHandler));echo('</pre>');
            echo('Get: config: <pre>');print_r($objWarnHandler->getTabConfig());echo('</pre>');
            echo('Get: key: <pre>');var_dump($objWarnHandler->getStrKey());echo('</pre>');
        }
        else
        {
            echo('Get: not found: <br />');
        }
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test handle warning
$tabWarning = array(
    array(
        ConstError::TAB_CONFIG_KEY_ERROR_TYPE => E_ERROR,
        ConstError::TAB_CONFIG_KEY_ERROR_MESSAGE => 'Error test',
        ConstError::TAB_CONFIG_KEY_FILE_PATH => __FILE__,
        ConstError::TAB_CONFIG_KEY_FILE_LINE_NUM => __LINE__
    ), // Ko
    ToolBoxErrorExceptionFactory::getObjErrorException(array(
        ConstError::TAB_CONFIG_KEY_ERROR_TYPE => E_WARNING,
        ConstError::TAB_CONFIG_KEY_ERROR_MESSAGE => 'Warning test',
        ConstError::TAB_CONFIG_KEY_FILE_PATH => __FILE__,
        ConstError::TAB_CONFIG_KEY_FILE_LINE_NUM => __LINE__
    )), // Ok: handle by error throwable warning handler
    array(
        ConstError::TAB_CONFIG_KEY_ERROR_TYPE => E_USER_ERROR,
        ConstError::TAB_CONFIG_KEY_ERROR_MESSAGE => 'User error test'
    ), // Ko
    ToolBoxErrorExceptionFactory::getObjErrorException(array(
        ConstError::TAB_CONFIG_KEY_ERROR_TYPE => E_USER_NOTICE,
        ConstError::TAB_CONFIG_KEY_ERROR_MESSAGE => 'User notice test'
    )), // Ok: handle by error throwable warning handler
    new Exception(
        'Exception 1 test',
        400
    ), // Ok: handle by throwable warning handlers
    new Exception(
        'Exception 2 test',
        500
    ) // Ok: handle by throwable warning handlers
);

$tabWarnHandlerWarning = array();
$tabWarnHandler = $objWarnHandlerCollection->getTabWarnHandler($tabWarning, $tabWarnHandlerWarning);
foreach($tabWarnHandler as $strKey => $warnHandler)
{
    $tabCurrentWarnHandlerWarning = $tabWarnHandlerWarning[$strKey];
    echo('Test warning : <pre>');var_dump($tabCurrentWarnHandlerWarning);echo('</pre>');

    /** @var WarnHandlerInterface $warnHandler */
    $warnHandler->execute($tabCurrentWarnHandlerWarning, null, true);

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


