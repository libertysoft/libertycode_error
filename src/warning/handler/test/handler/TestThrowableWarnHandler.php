<?php

namespace liberty_code\error\warning\handler\test\handler;

use liberty_code\error\warning\handler\throwable\fix\model\FixThrowableWarnHandler;

use Throwable;
use ErrorException;
use liberty_code\error\warning\handler\throwable\library\ConstThrowableWarnHandler;



class TestThrowableWarnHandler extends FixThrowableWarnHandler
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstThrowableWarnHandler::TAB_CONFIG_KEY_THROW_CLASS_PATH_EXCLUDE => [
                ErrorException::class
            ]
        );
    }





    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function execute(array $tabWarning, $resultExecution, $boolExecutionSuccess)
    {
        /** @var Throwable[] $tabWarning */
        echo('<br />Throwable (executed by ' . $this->getStrKey() . '): <pre>');
        $strListWarningClassPath = '';
        foreach($tabWarning as $warning)
        {
            $strListWarningClassPath .=
                ((trim($strListWarningClassPath) != '') ? ',' : '') .
                get_class($warning);

            var_dump(
                array(
                    'class' => get_class($warning),
                    'Code' => $warning->getCode(),
                    'Message' => $warning->getMessage()
                )
            );
        }
        echo('</pre>');

        return sprintf(
            '%1$s|throwable-warning:%2$s',
            (is_string($resultExecution) ? $resultExecution : '-'),
            $strListWarningClassPath
        );
    }



}