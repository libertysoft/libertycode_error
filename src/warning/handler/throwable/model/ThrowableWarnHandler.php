<?php
/**
 * Description :
 * This class allows to define throwable warning handler class.
 * Throwable warning handler allows to handle throwable warnings.
 *
 * Throwable warning handler uses the following specified configuration:
 * [
 *     Default warning handler configuration,
 *
 *     throw_class_path_include(optional): [
 *         "String throwable class path 1",
 *         ...,
 *         "String throwable class path N"
 *     ],
 *
 *     throw_class_path_exclude(optional): [
 *         "String throwable class path 1",
 *         ...,
 *         "String throwable class path N"
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\handler\throwable\model;

use liberty_code\error\warning\handler\model\DefaultWarnHandler;

use Throwable;
use liberty_code\error\warning\handler\library\ConstWarnHandler;
use liberty_code\error\warning\handler\throwable\library\ConstThrowableWarnHandler;
use liberty_code\error\warning\handler\throwable\exception\ConfigInvalidFormatException;



abstract class ThrowableWarnHandler extends DefaultWarnHandler
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstWarnHandler::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkCatches($warning)
    {
        $tabConfig = $this->getTabConfig();
        $tabThrowClassPathInclude = (
            isset($tabConfig[ConstThrowableWarnHandler::TAB_CONFIG_KEY_THROW_CLASS_PATH_INCLUDE]) ?
                $tabConfig[ConstThrowableWarnHandler::TAB_CONFIG_KEY_THROW_CLASS_PATH_INCLUDE] :
                null
        );
        $tabThrowClassPathExclude = (
            isset($tabConfig[ConstThrowableWarnHandler::TAB_CONFIG_KEY_THROW_CLASS_PATH_EXCLUDE]) ?
                $tabConfig[ConstThrowableWarnHandler::TAB_CONFIG_KEY_THROW_CLASS_PATH_EXCLUDE] :
                null
        );
        $result = (
            ($warning instanceof Throwable) &&
            (
                is_null($tabThrowClassPathInclude) ||
                (count(array_filter(
                    (is_array($tabThrowClassPathInclude) ? $tabThrowClassPathInclude : array()),
                    function($strClassPath) use ($warning) {
                        return ($warning instanceof $strClassPath);
                    }
                )) > 0)
            ) &&
            (
                is_null($tabThrowClassPathExclude) ||
                (count(array_filter(
                    (is_array($tabThrowClassPathExclude) ? $tabThrowClassPathExclude : array()),
                    function($strClassPath) use ($warning) {
                        return ($warning instanceof $strClassPath);
                    }
                )) == 0)
            )
        );

        // Return result
        return $result;
    }



}