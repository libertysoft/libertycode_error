<?php
/**
 * Description :
 * This class allows to define fixed throwable warning handler class.
 * Fixed throwable warning handler allows to handle throwable warnings,
 * from a specified fixed configuration.
 *
 * Fixed throwable warning handler uses the following specified configuration:
 * [
 *     Throwable warning handler configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\handler\throwable\fix\model;

use liberty_code\error\warning\handler\throwable\model\ThrowableWarnHandler;

use liberty_code\error\warning\handler\library\ConstWarnHandler;
use liberty_code\error\warning\handler\throwable\fix\exception\ConfigInvalidFormatException;



abstract class FixThrowableWarnHandler extends ThrowableWarnHandler
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        // Init var
        $tabConfig = $this->getTabFixConfig();

        // Call parent constructor
        parent::__construct($tabConfig);
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstWarnHandler::DATA_KEY_DEFAULT_CONFIG:
                    $tabConfig = $this->getTabFixConfig();
                    ConfigInvalidFormatException::setCheck($value, $tabConfig);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get fixed configuration array.
     *
     * @return array
     */
    abstract protected function getTabFixConfig();



}