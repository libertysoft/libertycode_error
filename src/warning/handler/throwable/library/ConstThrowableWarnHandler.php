<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\handler\throwable\library;



class ConstThrowableWarnHandler
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_THROW_CLASS_PATH_INCLUDE = 'throw_class_path_include';
    const TAB_CONFIG_KEY_THROW_CLASS_PATH_EXCLUDE = 'throw_class_path_exclude';
	
	
	
    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the throwable warning handler configuration standard.';
}