<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\handler\throwable\exception;

use Throwable;
use liberty_code\error\warning\handler\throwable\library\ConstThrowableWarnHandler;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstThrowableWarnHandler::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init throwable class path index array check function
        $checkTabThrowClassPathIsValid = function(array $tabThrowClassPath)
        {
            $result = true;

            // Check each throwable class path is valid
            for($intCpt = 0; ($intCpt < count($tabThrowClassPath)) && $result; $intCpt++)
            {
                $strThrowClassPathValue = $tabThrowClassPath[$intCpt];
                $result =
                    is_string($strThrowClassPathValue) &&
                    (trim($strThrowClassPathValue) != '') &&
                    (
                        class_exists($strThrowClassPathValue) ||
                        interface_exists($strThrowClassPathValue)
                    ) &&
                    is_subclass_of($strThrowClassPathValue, Throwable::class);
;
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid throwable class path included
            (
                (!isset($config[ConstThrowableWarnHandler::TAB_CONFIG_KEY_THROW_CLASS_PATH_INCLUDE])) ||
                (
                    is_array($config[ConstThrowableWarnHandler::TAB_CONFIG_KEY_THROW_CLASS_PATH_INCLUDE]) &&
                    $checkTabThrowClassPathIsValid($config[ConstThrowableWarnHandler::TAB_CONFIG_KEY_THROW_CLASS_PATH_INCLUDE])
                )
            ) &&

            // Check valid throwable class path excluded
            (
                (!isset($config[ConstThrowableWarnHandler::TAB_CONFIG_KEY_THROW_CLASS_PATH_EXCLUDE])) ||
                (
                    is_array($config[ConstThrowableWarnHandler::TAB_CONFIG_KEY_THROW_CLASS_PATH_EXCLUDE]) &&
                    $checkTabThrowClassPathIsValid($config[ConstThrowableWarnHandler::TAB_CONFIG_KEY_THROW_CLASS_PATH_EXCLUDE])
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}