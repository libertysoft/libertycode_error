<?php
/**
 * Description :
 * This class allows to define default warning handler factory class.
 * Can be consider is base of all warning handler factory type.
 *
 * Default warning handler factory uses the following specified configuration, to get and hydrate warning handler:
 * [
 *     file_path(optional): "file path to warning handler class",
 *
 *     OR
 *
 *     type(optional): "string constant to determine warning handler type",
 *
 *     ... specific warning handler configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\handler\factory\model;

use liberty_code\di\factory\model\DefaultFactory;
use liberty_code\error\warning\handler\factory\api\WarnHandlerFactoryInterface;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\library\reflection\library\ToolBoxFileReflection;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\error\warning\handler\library\ConstWarnHandler;
use liberty_code\error\warning\handler\api\WarnHandlerInterface;
use liberty_code\error\warning\handler\factory\library\ConstWarnHandlerFactory;
use liberty_code\error\warning\handler\factory\exception\FactoryInvalidFormatException;
use liberty_code\error\warning\handler\factory\exception\ConfigInvalidFormatException;



/**
 * @method null|WarnHandlerFactoryInterface getObjFactory() Get parent factory object.
 * @method void setObjFactory(null|WarnHandlerFactoryInterface $objFactory) Set parent factory object.
 */
class DefaultWarnHandlerFactory extends DefaultFactory implements WarnHandlerFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param WarnHandlerFactoryInterface $objFactory = null
     */
    public function __construct(
        WarnHandlerFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init handler factory
        $this->setObjFactory($objFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstWarnHandlerFactory::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstWarnHandlerFactory::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * Hydrate specified warning handler.
     * Overwrite it to set specific hydration.
     *
     * @param WarnHandlerInterface $objWarnHandler
     * @param array $tabConfigFormat
     */
    protected function hydrateWarnHandler(WarnHandlerInterface $objWarnHandler, array $tabConfigFormat)
    {
        // Init formatted configuration
        if(array_key_exists(ConstWarnHandlerFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            unset($tabConfigFormat[ConstWarnHandlerFactory::TAB_CONFIG_KEY_TYPE]);
        }
        if(array_key_exists(ConstWarnHandlerFactory::TAB_CONFIG_KEY_FILE_PATH, $tabConfigFormat))
        {
            unset($tabConfigFormat[ConstWarnHandlerFactory::TAB_CONFIG_KEY_FILE_PATH]);
        }

        // Hydrate warning handler
        if(count($tabConfigFormat) > 0)
        {
            $objWarnHandler->setConfig($tabConfigFormat);
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstWarnHandlerFactory::DATA_KEY_DEFAULT_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstWarnHandlerFactory::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified formatted configuration is valid,
     * for the specified warning handler object.
     *
     * @param WarnHandlerInterface $objWarnHandler
     * @param array $tabConfigFormat
     * @return boolean
     * @throws ConfigInvalidFormatException
     */
    protected function checkConfigIsValid(WarnHandlerInterface $objWarnHandler, array $tabConfigFormat)
    {
        // Init var
        $strWarnHandlerClassPath = $this->getStrWarnHandlerClassPathEngine($tabConfigFormat);
        $result = (
            (!is_null($strWarnHandlerClassPath)) &&
            ($strWarnHandlerClassPath == get_class($objWarnHandler))
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get formatted configuration array.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param string $strFilePath = null
     * @return array
     */
    protected function getTabConfigFormat(
        array $tabConfig,
        $strConfigKey = null,
        $strFilePath = null
    )
    {
        // Init var
        $result = $tabConfig;

        // Use configuration key, considered as warning handler key, if required
        if(
            (!is_null($strConfigKey)) &&
            (!array_key_exists(ConstWarnHandler::TAB_CONFIG_KEY_KEY, $result))
        )
        {
            $result[ConstWarnHandler::TAB_CONFIG_KEY_KEY] = $strConfigKey;
        }

        // Use file path, if required
        if(
            (!is_null($strFilePath)) &&
            (!array_key_exists(ConstWarnHandlerFactory::TAB_CONFIG_KEY_FILE_PATH, $result))
        )
        {
            $result[ConstWarnHandlerFactory::TAB_CONFIG_KEY_FILE_PATH] = $strFilePath;
        }

        // Return result
        return $result;
    }



    /**
     * Get string configured file path,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     */
    protected function getStrConfigFilePath(array $tabConfigFormat)
    {
        // Init var
        $result = null;

        // Get type, if found
        if(array_key_exists(ConstWarnHandlerFactory::TAB_CONFIG_KEY_FILE_PATH, $tabConfigFormat))
        {
            $result = $tabConfigFormat[ConstWarnHandlerFactory::TAB_CONFIG_KEY_FILE_PATH];
        }

        // Return result
        return $result;
    }



    /**
     * Get string configured type,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     */
    protected function getStrConfigType(array $tabConfigFormat)
    {
        // Init var
        $result = null;

        // Get type, if found
        if(array_key_exists(ConstWarnHandlerFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            $result = $tabConfigFormat[ConstWarnHandlerFactory::TAB_CONFIG_KEY_TYPE];
        }

        // Return result
        return $result;
    }



    /**
     * Get string class path of warning handler,
     * from specified configured file path.
     *
     * @param null|string $strConfigFilePath
     * @return null|string
     */
    protected function getStrWarnHandlerClassPathFromFilePath($strConfigFilePath)
    {
        // Init var
        $result = null;
        $tabClassPath = array_merge(
            ToolBoxFileReflection::getTabClassPath($strConfigFilePath),
            ToolBoxFileReflection::getTabInterfacePath($strConfigFilePath)
        );

        // Get class path, if required (first class path found)
        if(
            isset($tabClassPath[0]) &&
            is_subclass_of($tabClassPath[0], WarnHandlerInterface::class)
        )
        {
            $result = $tabClassPath[0];
        }

        // Return result
        return $result;
    }



    /**
     * Get string class path of warning handler,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|string
     */
    protected function getStrWarnHandlerClassPathFromType($strConfigType)
    {
        // Return result
        return null;
    }



    /**
     * Get string class path of warning handler engine,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrWarnHandlerClassPathEngine(array $tabConfigFormat)
    {
        // Check arguments
        ConfigInvalidFormatException::setCheck($tabConfigFormat);

        // Init var
        $strConfigFilePath = $this->getStrConfigFilePath($tabConfigFormat);
        $strConfigType = $this->getStrConfigType($tabConfigFormat);

        // Get class path
        $result = $this->getStrWarnHandlerClassPathFromFilePath($strConfigFilePath);
        if(is_null($result))
        {
            $result = $this->getStrWarnHandlerClassPathFromType($strConfigType);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getStrWarnHandlerClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getStrWarnHandlerClassPathEngine($tabConfigFormat);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrWarnHandlerClassPath($tabConfig, $strConfigKey);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getStrWarnHandlerClassPathFromFile($strFilePath, array $tabConfig = null)
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat(
            (is_null($tabConfig) ? array() : $tabConfig),
            null,
            $strFilePath
        );
        $result = $this->getStrWarnHandlerClassPathEngine($tabConfigFormat);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrWarnHandlerClassPathFromFile($strFilePath, $tabConfig);
        }

        // Return result
        return $result;
    }



    /**
     * Get new object instance warning handler,
     * from specified class path.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strClassPath
     * @return null|WarnHandlerInterface
     */
    protected function getObjWarnHandlerNew($strClassPath)
    {
        // Init instance
        $result = $this->getObjInstance($strClassPath);
        if(is_null($result))
        {
            $result = ToolBoxReflection::getObjInstance($strClassPath);
        }

        // Return result
        return $result;
    }



    /**
     * Get object instance warning handler engine.
     *
     * @param array $tabConfigFormat
     * @param WarnHandlerInterface $objWarnHandler = null
     * @return null|WarnHandlerInterface
     * @throws ConfigInvalidFormatException
     */
    protected function getObjWarnHandlerEngine(
        array $tabConfigFormat,
        WarnHandlerInterface $objWarnHandler = null
    )
    {
        // Init var
        $result = null;
        $strClassPath = $this->getStrWarnHandlerClassPathEngine($tabConfigFormat);
        $objWarnHandler = (
            is_null($objWarnHandler) ?
                $this->getObjWarnHandlerNew($strClassPath) :
                $objWarnHandler
        );

        // Get and hydrate warning handler, if required
        if(
            (!is_null($objWarnHandler)) &&
            $this->checkConfigIsValid($objWarnHandler, $tabConfigFormat)
        )
        {
            $this->hydrateWarnHandler($objWarnHandler, $tabConfigFormat);
            $result = $objWarnHandler;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getObjWarnHandler(
        array $tabConfig,
        $strConfigKey = null,
        WarnHandlerInterface $objWarnHandler = null
    )
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getObjWarnHandlerEngine($tabConfigFormat, $objWarnHandler);

        // Get warning handler from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjWarnHandler($tabConfig, $strConfigKey, $objWarnHandler);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getObjWarnHandlerFromFile(
        $strFilePath,
        array $tabConfig = null,
        WarnHandlerInterface $objWarnHandler = null
    )
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat(
            (is_null($tabConfig) ? array() : $tabConfig),
            null,
            $strFilePath
        );
        $result = $this->getObjWarnHandlerEngine($tabConfigFormat, $objWarnHandler);

        // Get warning handler from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjWarnHandlerFromFile($strFilePath, $tabConfig, $objWarnHandler);
        }

        // Return result
        return $result;
    }



}