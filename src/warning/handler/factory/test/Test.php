<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load test
require_once($strRootAppPath . '/src/warning/handler/factory/test/WarnHandlerFactoryTest.php');
require_once($strRootAppPath . '/src/warning/handler/test/handler/TestErrorThrowableWarnHandler.php');
require_once($strRootAppPath . '/src/warning/handler/test/handler/TestThrowableWarnHandler.php');
require_once($strRootAppPath . '/src/warning/handler/test/handler/TestThrowableWarnHandler2.php');

// Use test
use liberty_code\error\warning\handler\test\handler\TestErrorThrowableWarnHandler;
use liberty_code\error\warning\handler\test\handler\TestThrowableWarnHandler;
use liberty_code\error\warning\handler\test\handler\TestThrowableWarnHandler2;



// Init var
$objTestErrorThrowableWarnHandler = new TestErrorThrowableWarnHandler();
$objTestThrowableWarnHandler = new TestThrowableWarnHandler();
$objTestThrowableWarnHandler2 = new TestThrowableWarnHandler2();



// Test new warning handler
$tabWarnHandlerData = array(
    [
        array_merge(
            $objTestErrorThrowableWarnHandler->getTabConfig(),
            [
                'file_path' => $strRootAppPath . '/src/warning/handler/test/handler/TestErrorThrowableWarnHandler.php',
                'key' => 'error_throwable_warning_handler'
            ]
        ),
        'error_throwable_warning_handler_not_care',
        $objTestThrowableWarnHandler
    ], // Ko: not found: throwable warning handler used for error throwable warning handler config

    [
        array_merge(
            $objTestErrorThrowableWarnHandler->getTabConfig(),
            [
                'file_path' => $strRootAppPath . '/src/warning/handler/test/handler/TestErrorThrowableWarnHandler.php',
                'key' => 'error_throwable_warning_handler'
            ]
        ),
        'error_throwable_warning_handler_not_care',
        $objTestErrorThrowableWarnHandler
    ], // Ok

    [
        array_merge(
            $objTestThrowableWarnHandler->getTabConfig(),
            [
                'file_path' => $strRootAppPath . '/src/warning/handler/test/handler/TestThrowableWarnHandler.php',
                'key' => 'throwable_warning_handler'
            ]
        )
    ], // Ok

    [
        [
            'file_path' => $strRootAppPath . '/src/warning/handler/test/handler/TestThrowableWarnHandler2.php'
        ]
    ] // Ok
);

foreach($tabWarnHandlerData as $warnHandlerData)
{
    echo('Test new warning handler: <br />');
    echo('<pre>');var_dump($warnHandlerData);echo('</pre>');

    try{
        $tabConfig = $warnHandlerData[0];
        $strConfigKey = (isset($warnHandlerData[1]) ? $warnHandlerData[1] : null);
        $objWarnHandler = (isset($warnHandlerData[2]) ? $warnHandlerData[2] : null);
        $objWarnHandler = $objWarnHandlerFactory->getObjWarnHandler($tabConfig, $strConfigKey, $objWarnHandler);

        echo('Class path: <pre>');var_dump($objWarnHandlerFactory->getStrWarnHandlerClassPath($tabConfig, $strConfigKey));echo('</pre>');

        if(!is_null($objWarnHandler))
        {
            echo('Warning handler class path: <pre>');var_dump(get_class($objWarnHandler));echo('</pre>');
            echo('Warning handler config: <pre>');var_dump($objWarnHandler->getTabConfig());echo('</pre>');
            echo('Warning handler key: <pre>');var_dump($objWarnHandler->getStrKey());echo('</pre>');
        }
        else
        {
            echo('Warning handler not found<br />');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ':' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test new warning handler from file
$tabWarnHandlerData = array(
    [
        null,
        [
            'file_path' => $strRootAppPath . '/src/warning/handler/test/handler/TestErrorThrowableWarnHandler.php',
        ]
    ], // Ok

    [
        $strRootAppPath . '/src/warning/handler/test/handler/TestThrowableWarnHandler_not_care.php',
        array_merge(
            $objTestThrowableWarnHandler->getTabConfig(),
            [
                'file_path' => $strRootAppPath . '/src/warning/handler/test/handler/TestThrowableWarnHandler.php',
                'key' => 'throwable_warning_handler'
            ]
        ),
        $objTestThrowableWarnHandler2
    ], // Ko: not found: throwable warning handler 2 used for throwable warning handler config

    [
        $strRootAppPath . '/src/warning/handler/test/handler/TestThrowableWarnHandler_not_care.php',
        array_merge(
            $objTestThrowableWarnHandler->getTabConfig(),
            [
                'file_path' => $strRootAppPath . '/src/warning/handler/test/handler/TestThrowableWarnHandler.php',
                'key' => 'throwable_warning_handler'
            ]
        ),
        $objTestThrowableWarnHandler
    ], // Ok

    [
        $strRootAppPath . '/src/warning/handler/test/handler/TestThrowableWarnHandler2.php'
    ] // Ok
);

foreach($tabWarnHandlerData as $warnHandlerData)
{
    echo('Test new warning handler from file: <br />');
    echo('<pre>');var_dump($warnHandlerData);echo('</pre>');

    try{
        $strFilePath = $warnHandlerData[0];
        $tabConfig = (isset($warnHandlerData[1]) ? $warnHandlerData[1] : null);
        $objWarnHandler = (isset($warnHandlerData[2]) ? $warnHandlerData[2] : null);
        $objWarnHandler = $objWarnHandlerFactory->getObjWarnHandlerFromFile($strFilePath, $tabConfig, $objWarnHandler);

        echo('Class path: <pre>');var_dump($objWarnHandlerFactory->getStrWarnHandlerClassPathFromFile($strFilePath, $tabConfig));echo('</pre>');

        if(!is_null($objWarnHandler))
        {
            echo('Warning handler class path: <pre>');var_dump(get_class($objWarnHandler));echo('</pre>');
            echo('Warning handler config: <pre>');var_dump($objWarnHandler->getTabConfig());echo('</pre>');
            echo('Warning handler key: <pre>');var_dump($objWarnHandler->getStrKey());echo('</pre>');
        }
        else
        {
            echo('Warning handler not found<br />');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ':' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


