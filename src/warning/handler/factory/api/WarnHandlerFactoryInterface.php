<?php
/**
 * Description :
 * This class allows to describe behavior of warning handler factory class.
 * Warning handler factory allows to provide new or specified warning handler instance,
 * hydrated with a specified configuration,
 * from a set of potential predefined warning handler types or file path.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\handler\factory\api;

use liberty_code\error\warning\handler\api\WarnHandlerInterface;



interface WarnHandlerFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string class path of warning handler,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return null|string
     */
    public function getStrWarnHandlerClassPath(array $tabConfig, $strConfigKey = null);



    /**
     * Get string class path of warning handler,
     * from specified file path
     * and from specified configuration.
     *
     * @param string $strFilePath
     * @param array $tabConfig = null
     * @return null|string
     */
    public function getStrWarnHandlerClassPathFromFile($strFilePath, array $tabConfig = null);



    /**
     * Get new or specified object instance warning handler,
     * hydrated from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param WarnHandlerInterface $objWarnHandler = null
     * @return null|WarnHandlerInterface
     */
    public function getObjWarnHandler(
        array $tabConfig,
        $strConfigKey = null,
        WarnHandlerInterface $objWarnHandler = null
    );



    /**
     * Get new or specified object instance warning handler,
     * hydrated from specified file path,
     * and from specified configuration.
     *
     * @param string $strFilePath
     * @param array $tabConfig = null
     * @param WarnHandlerInterface $objWarnHandler = null
     * @return null|WarnHandlerInterface
     */
    public function getObjWarnHandlerFromFile(
        $strFilePath,
        array $tabConfig = null,
        WarnHandlerInterface $objWarnHandler = null
    );
}