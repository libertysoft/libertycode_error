<?php
/**
 * Description :
 * This class allows to define default warning handler class.
 * Can be consider is base of all warning handler type.
 *
 * Default warning handler uses the following specified configuration:
 * [
 *     key(optional: got static class name if not found): "string warning handler key"
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\handler\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\error\warning\handler\api\WarnHandlerInterface;

use liberty_code\library\reflection\library\ToolBoxClassReflection;
use liberty_code\error\warning\handler\library\ConstWarnHandler;
use liberty_code\error\warning\handler\exception\ConfigInvalidFormatException;



abstract class DefaultWarnHandler extends FixBean implements WarnHandlerInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     */
    public function __construct(array $tabConfig = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstWarnHandler::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstWarnHandler::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstWarnHandler::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstWarnHandler::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if specified warning is catch by this handler.
     * Overwrite it to set specific feature.
     *
     * @param mixed $warning
     * @return boolean
     */
    public function checkCatches($warning)
    {
        return true;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Return result
        return $this->beanGet(ConstWarnHandler::DATA_KEY_DEFAULT_CONFIG);
    }
	
	

    /**
     * @inheritdoc
     */
    public function getStrKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstWarnHandler::TAB_CONFIG_KEY_KEY, $tabConfig) ?
                $tabConfig[ConstWarnHandler::TAB_CONFIG_KEY_KEY] :
                ToolBoxClassReflection::getStrClassName(static::class)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * Overwrite it to set specific feature.
     */
    public function getTabCatchWarning(array $tabWarning)
    {
        // Init var
        $result = array();

        // Run each warning
        foreach($tabWarning as $warning)
        {
            // Get warning, if required
            if($this->checkCatches($warning))
            {
                $result[] = $warning;
            }
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setConfig(array $tabConfig)
    {
        $this->beanSet(ConstWarnHandler::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }



}