<?php
/**
 * Description :
 * This class allows to define default warning handler collection class.
 * key: warning handler key => warning handler.
 *
 * Default warning handler collection uses the following specified configuration:
 * [
 *     multi_catch_require(optional: got true if not found): true / false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\handler\model;

use liberty_code\library\bean\model\DefaultBean;
use liberty_code\error\warning\handler\api\WarnHandlerCollectionInterface;

use liberty_code\library\bean\library\ConstBean;
use liberty_code\error\warning\handler\library\ConstWarnHandler;
use liberty_code\error\warning\handler\api\WarnHandlerInterface;
use liberty_code\error\warning\handler\exception\CollectionConfigInvalidFormatException;
use liberty_code\error\warning\handler\exception\CollectionKeyInvalidFormatException;
use liberty_code\error\warning\handler\exception\CollectionValueInvalidFormatException;



class DefaultWarnHandlerCollection extends DefaultBean implements WarnHandlerCollectionInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /** @var array */
    protected $tabConfig;




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     */
    public function __construct(
        array $tabConfig = null,
        array $tabData = array()
    )
    {
        // Init var
        $this->tabConfig = array();

        // Call parent constructor
        parent::__construct($tabData);

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            // Check value argument
            CollectionValueInvalidFormatException::setCheck($value);

            // Check key argument
            /** @var WarnHandlerInterface $value */
            if(
                (!is_string($key)) ||
                ($key != $value->getStrKey())
            )
            {
                throw new CollectionKeyInvalidFormatException($key);
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }
	
	
	
	
	
	// Methods check
	// ******************************************************************************

    /**
     * Check multi catch required.
     *
     * @return boolean
     */
    protected function checkMultiCatchRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstWarnHandler::TAB_COLLECTION_CONFIG_KEY_MULTI_CATCH_REQUIRE])) ||
            (intval($tabConfig[ConstWarnHandler::TAB_COLLECTION_CONFIG_KEY_MULTI_CATCH_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function checkExists($strKey)
    {
        // Return result
        return (!is_null($this->getObjWarnHandler($strKey)));
    }
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig()
    {
        // Return result
        return $this->tabConfig;
    }



    /**
     * @inheritdoc
     */
    public function getTabKey()
    {
        // Return result
        return $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY);
    }



    /**
     * @inheritdoc
     */
    public function getObjWarnHandler($strKey)
    {
        // Init var
        $result = null;

        // Try to get handler object, if found
        try
        {
            if($this->beanDataExists($strKey))
            {
                $result = $this->beanGetData($strKey);
            }
        }
        catch(\Exception $e)
        {
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabWarnHandler(array $tabWarning, array &$tabWarnHandlerWarning = array())
    {
        // Init var
        $result = array();
        $tabWarnHandlerWarning = array();
        $boolMultiCatch = $this->checkMultiCatchRequired();

        // Run all warning handlers
        /** @var WarnHandlerInterface[] $tabWarnHandler */
        $tabWarnHandler = $this->__beanTabData; // Perf requirement: Get directly array of warning handlers
        foreach($tabWarnHandler as $strKey => $objWarnHandler)
        {
            // Get warning handler, required
            $tabCurrentWarnHandlerWarning = $objWarnHandler->getTabCatchWarning($tabWarning);
            if(count($tabCurrentWarnHandlerWarning) > 0)
            {
                // Set warning handler on result
                $result[$strKey] = $objWarnHandler;
                $tabWarnHandlerWarning[$strKey] = $tabCurrentWarnHandlerWarning;

                // Break, if required
                if(!$boolMultiCatch) break;
            }
        }

        // Return result
        return $result;
    }



	
	
	// Methods setters
	// ******************************************************************************

    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     * @throws CollectionConfigInvalidFormatException
     */
    public function setConfig(array $tabConfig)
    {
        // Set check argument
        CollectionConfigInvalidFormatException::setCheck($tabConfig);

        $this->tabConfig = $tabConfig;
    }



	/**
	 * @inheritdoc
	 * @throws CollectionKeyInvalidFormatException
	 * @throws CollectionValueInvalidFormatException
     */
	public function setWarnHandler(WarnHandlerInterface $objWarnHandler)
	{
		// Init var
		$strKey = $objWarnHandler->getStrKey();
		
		// Register instance
		$this->beanPutData($strKey, $objWarnHandler);
		
		// return result
		return $strKey;
	}



    /**
     * @inheritdoc
     */
    public function setTabWarnHandler($tabWarnHandler)
    {
        // Init var
        $result = array();

        // Case index array of warning handlers
        if(is_array($tabWarnHandler))
        {
            // Run all warning handlers and for each, try to set
            foreach($tabWarnHandler as $warnHandler)
            {
                $strKey = $this->setWarnHandler($warnHandler);
                $result[] = $strKey;
            }
        }
        // Case collection of warning handlers
        else if($tabWarnHandler instanceof WarnHandlerCollectionInterface)
        {
            // Run all warning handlers and for each, try to set
            foreach($tabWarnHandler->getTabKey() as $strKey)
            {
                $objWarnHandler = $tabWarnHandler->getObjWarnHandler($strKey);
                $strKey = $this->setWarnHandler($objWarnHandler);
                $result[] = $strKey;
            }
        }

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeWarnHandler($strKey)
    {
        // Init var
        $result = $this->getObjWarnHandler($strKey);

        // Remove route
        $this->beanRemoveData($strKey);

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeWarnHandlerAll()
    {
        // Ini var
        $tabKey = $this->getTabKey();

        foreach($tabKey as $strKey)
        {
            $this->removeWarnHandler($strKey);
        }
    }



}