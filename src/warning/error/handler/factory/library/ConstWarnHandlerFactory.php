<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\error\handler\factory\library;



class ConstWarnHandlerFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_WARN_REGISTER = 'objWarnRegister';



    // Type configuration
    const CONFIG_TYPE_WARN_ERROR = 'warn_error';
}