<?php
/**
 * Description :
 * This class allows to define warning handler factory class.
 * Warning handler factory allows to provide and hydrate warning error handler instance.
 *
 * Warning handler factory uses the following specified configuration, to get and hydrate handler:
 * [
 *     -> Configuration key(optional): "handler key"
 *     type(required): "warn_error",
 *     Warning error handler configuration (@see WarnErrorHandler )
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\error\handler\factory\model;

use liberty_code\error\handler\factory\model\DefaultHandlerFactory;

use liberty_code\register\register\api\RegisterInterface;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\error\handler\api\HandlerInterface;
use liberty_code\error\handler\factory\api\HandlerFactoryInterface;
use liberty_code\error\warning\tryer\exception\WarnRegisterInvalidFormatException;
use liberty_code\error\warning\error\handler\error\model\WarnErrorHandler;
use liberty_code\error\warning\error\handler\factory\library\ConstWarnHandlerFactory;



/**
 * @method RegisterInterface getObjWarnRegister() Get warning register object.
 * @method void setObjWarnRegister(RegisterInterface $objWarnRegister) Set warning register object.
 */
class WarnHandlerFactory extends DefaultHandlerFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************


    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RegisterInterface $objWarnRegister
     */
    public function __construct(
        RegisterInterface $objWarnRegister,
        HandlerFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objFactory,
            $objProvider
        );

        // Init warning register
        $this->setObjWarnRegister($objWarnRegister);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstWarnHandlerFactory::DATA_KEY_WARN_REGISTER))
        {
            $this->__beanTabData[ConstWarnHandlerFactory::DATA_KEY_WARN_REGISTER] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * @inheritdoc
     */
    protected function hydrateHandler(HandlerInterface $objHandler, array $tabConfigFormat)
    {
        // Hydrate warning error handler, if required
        if($objHandler instanceof WarnErrorHandler)
        {
            $objHandler->setObjWarnRegister($this->getObjWarnRegister());
        }

        // Call parent method
        parent::hydrateHandler($objHandler, $tabConfigFormat);
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstWarnHandlerFactory::DATA_KEY_WARN_REGISTER
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstWarnHandlerFactory::DATA_KEY_WARN_REGISTER:
                    WarnRegisterInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrHandlerClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of handler, from type
        switch($strConfigType)
        {
            case null:
            case ConstWarnHandlerFactory::CONFIG_TYPE_WARN_ERROR:
                $result = WarnErrorHandler::class;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getObjHandlerNew($strClassPath)
    {
        // Init var
        $result = null;
        $objWarnRegister = $this->getObjWarnRegister();

        // Get handler instance, from class
        if(is_string($strClassPath))
        {
            switch($strClassPath)
            {
                case WarnErrorHandler::class:
                    $result = new WarnErrorHandler($objWarnRegister);
                    break;
            }
        }

        // Return result
        return $result;
    }



}