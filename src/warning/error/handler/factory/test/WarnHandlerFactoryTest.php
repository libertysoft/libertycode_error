<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../../..';

// Load test
require_once($strRootAppPath . '/src/handler/factory/test/HandlerFactoryTest.php');

// Use
use liberty_code\register\register\api\RegisterInterface;
use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\di\dependency\preference\model\Preference;
use liberty_code\error\warning\error\handler\factory\model\WarnHandlerFactory;



// Init DI
$objWarnRegister = new DefaultTableRegister();
$objPref = new Preference(array(
    'source' => RegisterInterface::class,
    'set' =>  ['type' => 'instance', 'value' => $objWarnRegister],
    'option' => [
        'shared' => true
    ]
));
$objDepCollection->setDependency($objPref);



// Init var
$objHandlerFactory = new WarnHandlerFactory($objWarnRegister, $objHandlerFactory, $objProvider);


