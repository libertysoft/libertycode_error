<?php
/**
 * Description :
 * This class allows to define warning error handler class.
 * Warning error handler is error handler,
 * which put error exception, on specific warning register.
 *
 * Warning error handler uses the following specified configuration:
 * [
 *     Error handler configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\error\handler\error\model;

use liberty_code\error\handler\error\model\ErrorHandler;

use liberty_code\register\register\api\RegisterInterface;
use liberty_code\error\warning\library\ToolBoxWarnRegister;
use liberty_code\error\warning\tryer\exception\WarnRegisterInvalidFormatException;
use liberty_code\error\warning\error\handler\error\library\ConstWarnErrorHandler;



/**
 * @method RegisterInterface getObjWarnRegister() Get warning register object.
 * @method void setObjWarnRegister(RegisterInterface $objWarnRegister) Set warning register object.
 */
class WarnErrorHandler extends ErrorHandler
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RegisterInterface $objWarnRegister
     */
    public function __construct(
        RegisterInterface $objWarnRegister,
        array $tabConfig = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $tabConfig
        );

        // Init warning register
        $this->setObjWarnRegister($objWarnRegister);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstWarnErrorHandler::DATA_KEY_WARN_REGISTER))
        {
            $this->__beanTabData[ConstWarnErrorHandler::DATA_KEY_WARN_REGISTER] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstWarnErrorHandler::DATA_KEY_WARN_REGISTER
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstWarnErrorHandler::DATA_KEY_WARN_REGISTER:
                    WarnRegisterInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function execute($error)
    {
        // Init var
        $objWarnRegister = $this->getObjWarnRegister();
        $objErrorException = $this->getObjErrorException($error);

        // Put error exception on warning register, if required
        if(!is_null($objErrorException))
        {
            ToolBoxWarnRegister::putThrowable($objWarnRegister, $objErrorException);
        }
    }



}