<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\error\handler\error\library;



class ConstWarnErrorHandler
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_WARN_REGISTER = 'objWarnRegister';
}