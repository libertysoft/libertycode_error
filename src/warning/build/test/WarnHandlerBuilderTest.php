<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/warning/handler/factory/test/WarnHandlerFactoryTest.php');

// Use
use liberty_code\error\warning\build\model\DefaultBuilder;
use liberty_code\error\warning\build\directory\model\DirBuilder;

$objWarnHandlerBuilder = new DefaultBuilder(
    $objWarnHandlerFactory
);

$objWarnHandlerDirBuilder = new DirBuilder(
    $objWarnHandlerFactory
);


