<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/warning/build/test/WarnHandlerBuilderTest.php');
require_once($strRootAppPath . '/src/warning/handler/test/handler/TestErrorThrowableWarnHandler.php');
require_once($strRootAppPath . '/src/warning/handler/test/handler/TestThrowableWarnHandler.php');
require_once($strRootAppPath . '/src/warning/handler/test/handler/TestThrowableWarnHandler2.php');

// Use
use liberty_code\error\warning\handler\api\WarnHandlerInterface;
use liberty_code\error\warning\handler\model\DefaultWarnHandlerCollection;

// Use test
use liberty_code\error\warning\handler\test\handler\TestErrorThrowableWarnHandler;
use liberty_code\error\warning\handler\test\handler\TestThrowableWarnHandler;
use liberty_code\error\warning\handler\test\handler\TestThrowableWarnHandler2;



// Init var
$objTestErrorThrowableWarnHandler = new TestErrorThrowableWarnHandler();
$objTestThrowableWarnHandler = new TestThrowableWarnHandler();
$objTestThrowableWarnHandler2 = new TestThrowableWarnHandler2();
$objWarnHandlerCollection = new DefaultWarnHandlerCollection();

$tabDataSrc = array(
    'error_throwable_warning_handler' =>  array_merge(
        $objTestErrorThrowableWarnHandler->getTabConfig(),
        [
            'file_path' => $strRootAppPath . '/src/warning/handler/test/handler/TestErrorThrowableWarnHandler.php'
        ]
    ),
    [
        'file_path' => $strRootAppPath . '/src/warning/handler/test/handler/TestThrowableWarnHandler.php',
    ],
    array_merge(
        $objTestThrowableWarnHandler2->getTabConfig(),
        [
            'file_path' => $strRootAppPath . '/src/warning/handler/test/handler/TestThrowableWarnHandler2.php',
            'key' => 'throwable_warning_handler_2'
        ]
    )
);



// Test properties
echo('Test properties: <br />');
echo('Data source initialization: <pre>');print_r($objWarnHandlerBuilder->getTabDataSrc());echo('</pre>');

echo('<br />');

$objWarnHandlerBuilder->setTabDataSrc($tabDataSrc);
echo('Data source hydrated: <pre>');print_r($objWarnHandlerBuilder->getTabDataSrc());echo('</pre>');

echo('<br />');

echo('<br /><br /><br />');



// Test hydrate warning handler collection
echo('Test hydrate warning handler collection: <br />');

$objWarnHandlerBuilder->hydrateWarnHandlerCollection($objWarnHandlerCollection, false);
foreach($objWarnHandlerCollection as $strKey => $objWarnHandler)
{
    /** @var WarnHandlerInterface $objWarnHandler */
	try{
		echo('Warning handler "' . $strKey . '":');echo('<br />');
        echo('Get: class: <pre>');print_r(get_class($objWarnHandler));echo('</pre>');
        echo('Get: config: <pre>');print_r($objWarnHandler->getTabConfig());echo('</pre>');
        echo('Get: key: <pre>');var_dump($objWarnHandler->getStrKey());echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	
	echo('<br /><br />');
}

echo('<br /><br /><br />');



// Test clear warning handler collection
echo('Test clear: <br />');

echo('Before clearing: <pre>');print_r($objWarnHandlerCollection->getTabKey());echo('</pre>');

$objWarnHandlerBuilder->setTabDataSrc(array());
$objWarnHandlerBuilder->hydrateWarnHandlerCollection($objWarnHandlerCollection);
echo('After clearing: <pre>');print_r($objWarnHandlerCollection->getTabKey());echo('</pre>');

echo('<br /><br /><br />');



// Test hydrate warning handler collection (with directory)
$tabDataSrc = array(
    $strRootAppPath . '/src/warning/handler/test/handler'
);

echo('Test hydrate warning handler collection (with directory): <br />');

$objWarnHandlerDirBuilder->setTabDataSrc($tabDataSrc);
$objWarnHandlerDirBuilder->hydrateWarnHandlerCollection($objWarnHandlerCollection, false);
foreach($objWarnHandlerCollection as $strKey => $objWarnHandler)
{
    /** @var WarnHandlerInterface $objWarnHandler */
    try{
        echo('Warning handler "' . $strKey . '":');echo('<br />');
        echo('Get: class: <pre>');print_r(get_class($objWarnHandler));echo('</pre>');
        echo('Get: config: <pre>');print_r($objWarnHandler->getTabConfig());echo('</pre>');
        echo('Get: key: <pre>');var_dump($objWarnHandler->getStrKey());echo('</pre>');
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }

    echo('<br /><br />');
}

echo('<br /><br /><br />');


