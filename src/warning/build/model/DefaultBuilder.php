<?php
/**
 * Description :
 * This class allows to define default builder class.
 * Default builder allows to populate warning handler collection,
 * from a specified array of source data.
 *
 * Default builder uses the following specified source data, to hydrate warning handler collection:
 * [
 *     // Warning handler configuration 1
 *     Warning handler factory configuration (@see WarnHandlerFactoryInterface ),
 *
 *     ...,
 *
 *     // Warning handler configuration N
 *     ...
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\build\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\error\warning\build\api\BuilderInterface;

use liberty_code\error\warning\handler\api\WarnHandlerCollectionInterface;
use liberty_code\error\warning\handler\factory\api\WarnHandlerFactoryInterface;
use liberty_code\error\warning\build\library\ConstBuilder;
use liberty_code\error\warning\build\exception\FactoryInvalidFormatException;
use liberty_code\error\warning\build\exception\DataSrcInvalidFormatException;



/**
 * @method null|WarnHandlerFactoryInterface getObjFactory() Get warning handler factory object.
 * @method void setObjFactory(null|WarnHandlerFactoryInterface $objFactory) Set warning handler factory object.
 * @method array getTabDataSrc() get data source array.
 * @method void setTabDataSrc(array $tabDataSrc) Set data source array.
 */
class DefaultBuilder extends FixBean implements BuilderInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param WarnHandlerFactoryInterface $objFactory
     * @param array $tabDataSrc = array()
     */
    public function __construct(
        WarnHandlerFactoryInterface $objFactory,
        array $tabDataSrc = array()
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init warning handler factory
        $this->setObjFactory($objFactory);

        // Init data source
        $this->setTabDataSrc($tabDataSrc);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->beanAdd(ConstBuilder::DATA_KEY_DEFAULT_FACTORY, null);
        }

        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC))
        {
            $this->beanAdd(ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC, array());
        }
    }



    /**
     * @inheritdoc
     * @throws DataSrcInvalidFormatException
     */
    public function hydrateWarnHandlerCollection(WarnHandlerCollectionInterface $objWarnHandlerCollection, $boolClear = true)
    {
        // Init var
        $boolClear = (is_bool($boolClear) ? $boolClear : true);
        $objFactory = $this->getObjFactory();
        $tabDataSrc = $this->getTabDataSrc();

        // Run each data source
        $tabWarnHandler = array();
        foreach($tabDataSrc as $key => $tabConfig)
        {
            // Get new warning handler
            $key = (is_string($key) ? $key : null);
            $objWarnHandler = $objFactory->getObjWarnHandler($tabConfig, $key);

            // Register warning handler, if found
            if(!is_null($objWarnHandler))
            {
                $tabWarnHandler[] = $objWarnHandler;
            }
            // Throw exception if warning handler not found, from data source
            else
            {
                throw new DataSrcInvalidFormatException(serialize($tabDataSrc));
            }
        }

        // Clear warning handlers from collection, if required
        if($boolClear)
        {
            $objWarnHandlerCollection->removeWarnHandlerAll();
        }

        // Register warning handlers on collection
        $objWarnHandlerCollection->setTabWarnHandler($tabWarnHandler);
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
            ConstBuilder::DATA_KEY_DEFAULT_FACTORY,
            ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstBuilder::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

				case ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC:
                    DataSrcInvalidFormatException::setCheck($value);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}
	
	
	
}