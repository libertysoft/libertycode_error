<?php
/**
 * Description :
 * This class allows to define directory builder class.
 * Directory builder allows to populate warning handler collection,
 * from a specified array of source data.
 *
 * Directory builder uses the following specified source data, to hydrate warning handler collection:
 * [
 *     "String unique warning handler directory path 1:
 *      Each php file considered as warning handler class"
 *
 *     ...,
 *
 *     "String unique warning handler directory path N"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\build\directory\model;

use liberty_code\error\warning\build\model\DefaultBuilder;

use liberty_code\error\warning\handler\api\WarnHandlerCollectionInterface;
use liberty_code\error\warning\build\library\ConstBuilder;
use liberty_code\error\warning\build\directory\exception\DataSrcInvalidFormatException;



class DirBuilder extends DefaultBuilder
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws DataSrcInvalidFormatException
     */
    public function hydrateWarnHandlerCollection(WarnHandlerCollectionInterface $objWarnHandlerCollection, $boolClear = true)
    {
        // Init var
        $boolClear = (is_bool($boolClear) ? $boolClear : true);
        $objFactory = $this->getObjFactory();
        $tabDataSrc = $this->getTabDataSrc();

        // Run each data source
        $tabWarnHandler = array();
        foreach($tabDataSrc as $strDirPath)
        {
            $tabFilePath = glob($strDirPath . '/*.php');

            // Run all files
            foreach($tabFilePath as $strFilePath)
            {
                // Check valid file path
                if(is_file($strFilePath) && is_readable($strFilePath))
                {
                    // Get new warning handler
                    $objWarnHandler = $objFactory->getObjWarnHandlerFromFile($strFilePath);

                    // Register warning handler, if found
                    if(!is_null($objWarnHandler))
                    {
                        $tabWarnHandler[] = $objWarnHandler;
                    }
                    // Throw exception if warning handler not found, from data source
                    else
                    {
                        throw new DataSrcInvalidFormatException(serialize($tabDataSrc));
                    }
                }
            }
        }

        // Clear warning handlers from collection, if required
        if($boolClear)
        {
            $objWarnHandlerCollection->removeWarnHandlerAll();
        }

        // Register warning handlers on collection
        $objWarnHandlerCollection->setTabWarnHandler($tabWarnHandler);
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC:
                    DataSrcInvalidFormatException::setCheck($value);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}
	
	
	
}