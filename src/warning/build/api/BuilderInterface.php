<?php
/**
 * Description :
 * This class allows to describe behavior of builder class.
 * Builder allows to populate specified warning handler collection instance, with warning handlers.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\build\api;

use liberty_code\error\warning\handler\api\WarnHandlerCollectionInterface;



interface BuilderInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified warning handler collection.
     *
     * @param WarnHandlerCollectionInterface $objWarnHandlerCollection
     * @param boolean $boolClear = true
     */
    public function hydrateWarnHandlerCollection(WarnHandlerCollectionInterface $objWarnHandlerCollection, $boolClear = true);
}