<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\library;

use Throwable;
use liberty_code\library\crypto\library\ToolBoxHash;
use liberty_code\register\register\api\RegisterInterface;
use liberty_code\error\warning\library\ConstWarning;



class ToolBoxWarnRegister
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods setters
    // ******************************************************************************

    /**
     * Put specified throwable,
     * on specified warning register.
     * Return string register key if success, false else.
     *
     * Configuration format:
     * @see RegisterInterface::putItem() configuration format.
     *
     * @param RegisterInterface $objWarnRegister
     * @param Throwable $objThrowable
     * @param string $strKey = null
     * @param array $tabConfig = null
     * @return boolean|string
     */
    public static function putThrowable(
        RegisterInterface $objWarnRegister,
        Throwable $objThrowable,
        $strKey = null,
        array $tabConfig = null
    )
    {
        // Init var
        $strKey = (
            (!is_null($strKey)) ?
                $strKey :
                sprintf(
                    ConstWarning::CONF_REGISTER_HASH_PATTERN,
                    ToolBoxHash::getStrHash($objThrowable)
                )
        );
        $result = (
            $objWarnRegister->putItem($strKey, $objThrowable, $tabConfig) ?
                $strKey :
                false
        );

        // Return result
        return $result;
    }



    /**
     * Remove specified throwable,
     * from specified warning register.
     *
     * @param RegisterInterface $objWarnRegister
     * @param string|Throwable $throwable : string throwable key|throwable object
     * @return boolean
     */
    public static function removeThrowable(
        RegisterInterface $objWarnRegister,
        $throwable
    )
    {
        // Init var
        $strKey = (
            ($throwable instanceof Throwable) ?
                sprintf(
                    ConstWarning::CONF_REGISTER_HASH_PATTERN,
                    ToolBoxHash::getStrHash($throwable)
                ) :
                $throwable
        );

        // Return result
        return $objWarnRegister->removeItem($strKey);
    }



}