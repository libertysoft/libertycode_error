<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\library;



class ConstWarning
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const CONF_REGISTER_HASH_PATTERN = 'warning_%1$s';
}