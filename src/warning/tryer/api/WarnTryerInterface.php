<?php
/**
 * Description :
 * This class allows to describe behavior of warning tryer class.
 * Warning tryer allows to try execution of specific process
 * and handle potential warnings after this process succeeded or failed (errors occurs),
 * using warning handler collection.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\tryer\api;

use liberty_code\error\tryer\api\TryerInterface;

use liberty_code\error\warning\handler\api\WarnHandlerCollectionInterface;



interface WarnTryerInterface extends TryerInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get warning handler collection object.
     *
     * @return WarnHandlerCollectionInterface
     */
    public function getObjWarnHandlerCollection();





    // Methods setters
    // ******************************************************************************

    /**
     * Set warning handler collection object.
     *
     * @param WarnHandlerCollectionInterface $objWarnHandlerCollection
     */
    public function setWarnHandlerCollection(WarnHandlerCollectionInterface $objWarnHandlerCollection);
}