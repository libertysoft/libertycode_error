<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/warning/tryer/test/WarnTryerTest.php');



// Test warning tryer execution
$tabCallableExecution = array(
    function(){
        trigger_error('User warning test', E_USER_WARNING);
        trigger_error('User notice test', E_USER_NOTICE);
        trigger_error('User error test', E_USER_ERROR);

        return 'Execution done';
    }, // Ko: Error handle

    function() use ($objWarnTryer) {
        $objWarnTryer->putWarnThrowable(new Exception('Throwable warning test'));
        $objWarnTryer->putWarnThrowable(new Exception('Throwable warning test 2'));

        throw new Exception(
            'Exception 1 test',
            400
        );

        return 'Execution done';
    }, // Ko: Throwable handle

    function() use ($objWarnTryer) {
        trigger_error('User warning test', E_USER_WARNING);
        trigger_error('User notice test', E_USER_NOTICE);

        $objWarnTryer->putWarnThrowable(new Exception('Throwable warning test'));
        $objWarnTryer->putWarnThrowable(new Exception('Throwable warning test 2'));

        return 'Execution done';
    }, // Ok

    function(){
        trigger_error('User error test', E_USER_ERROR);

        return 'Execution done';
    }, // Ko: Error handle

    function() use ($objWarnTryer) {
        throw new Exception(
            'Exception 2 test',
            500
        );

        return 'Execution done';
    }, // Ko: Throwable handle

    function(){
        return 'Execution done';
    }, // Ok
);

foreach($tabCallableExecution as $callableExecution)
{
    echo('Test warning tryer execution: <br />');

	try{
        $boolExecutionSuccess = false;
        $resultExecution = $objWarnTryer->execute($callableExecution, $boolExecutionSuccess);

        echo('Execution result: <pre>');print_r($resultExecution);echo('</pre>');
        echo('Execution success: <pre>');var_dump($boolExecutionSuccess);echo('</pre>');

	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



trigger_error('User warning test', E_USER_WARNING);
trigger_error('User notice test', E_USER_NOTICE);
trigger_error('User error test', E_USER_ERROR);


