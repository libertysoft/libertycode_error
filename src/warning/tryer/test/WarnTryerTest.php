<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/tryer/test/TryerTest.php');
require_once($strRootAppPath . '/src/warning/build/test/WarnHandlerBuilderTest.php');
require_once($strRootAppPath . '/src/warning/error/handler/factory/test/WarnHandlerFactoryTest.php');

// Use
use liberty_code\error\warning\handler\model\DefaultWarnHandlerCollection;
use liberty_code\error\warning\tryer\library\ConstWarnTryer;
use liberty_code\error\warning\tryer\model\DefaultWarnTryer;



// Hydrate builders
$objHandlerBuilder->setObjFactory($objHandlerFactory);
$objHandlerDirBuilder->setObjFactory($objHandlerFactory);



// Init var
$tabConfig = array(
    ConstWarnTryer::TAB_CONFIG_KEY_WARN_REGISTER_CLEAR_REQUIRE => true
);
$objWarnHandlerCollection = new DefaultWarnHandlerCollection();
$objWarnTryer = new DefaultWarnTryer(
    $objHandlerCollection,
    $objWarnHandlerCollection,
    $objWarnRegister,
    $tabConfig
);



// Hydrate handler collection (with waning error handler)
$tabDataSrc = array(
    [
        'type' => 'warn_error',
        'error_type_include' => [
            E_WARNING,
            E_NOTICE,
            E_CORE_WARNING,
            E_COMPILE_WARNING,
            E_USER_WARNING,
            E_USER_NOTICE,
            E_RECOVERABLE_ERROR
        ]
    ]
);
$objHandlerBuilder->setTabDataSrc($tabDataSrc);
$objHandlerBuilder->hydrateHandlerCollection($objHandlerCollection, false);



// Hydrate warning handler collection
$tabDataSrc = array(
    $strRootAppPath . '/src/warning/handler/test/handler'
);

$objWarnHandlerDirBuilder->setTabDataSrc($tabDataSrc);
$objWarnHandlerDirBuilder->hydrateWarnHandlerCollection($objWarnHandlerCollection);


