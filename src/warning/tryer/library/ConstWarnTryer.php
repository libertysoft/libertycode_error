<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\tryer\library;



class ConstWarnTryer
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
	const DATA_KEY_DEFAULT_WARN_HANDLER_COLLECTION = 'objWarnHandlerCollection';
    const DATA_KEY_DEFAULT_WARN_REGISTER = 'objWarnRegister';



    // Configuration
    const TAB_CONFIG_KEY_WARN_EXECUTION_RETURN_REQUIRE = 'warn_execution_return_require';
    const TAB_CONFIG_KEY_SELECT_WARN_EXECUTION_RETURN = 'select_warn_execution_return';
    const TAB_CONFIG_KEY_WARN_REGISTER_CLEAR_REQUIRE = 'warn_register_clear_require';

    // Warning execution return selection configuration
    const CONFIG_SELECT_WARN_EXECUTION_RETURN_FIRST = 'first';
    const CONFIG_SELECT_WARN_EXECUTION_RETURN_LAST = 'last';



    // Exception message constants
    const EXCEPT_MSG_WARN_HANDLER_COLLECTION_INVALID_FORMAT =
        'Following warning handler collection "%1$s" invalid! 
        It must be a warning handler collection object.';
    const EXCEPT_MSG_WARN_REGISTER_INVALID_FORMAT =
        'Following warning register "%1$s" invalid! 
        It must be a register object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default warning tryer configuration standard.';
}