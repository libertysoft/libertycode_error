<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\tryer\exception;

use liberty_code\error\warning\handler\api\WarnHandlerCollectionInterface;
use liberty_code\error\warning\tryer\library\ConstWarnTryer;



class WarnHandlerCollectionInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $warnHandlerCollection
     */
	public function __construct($warnHandlerCollection)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstWarnTryer::EXCEPT_MSG_WARN_HANDLER_COLLECTION_INVALID_FORMAT,
            mb_strimwidth(strval($warnHandlerCollection), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified warning handler collection has valid format.
	 * 
     * @param mixed $warnHandlerCollection
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($warnHandlerCollection)
    {
		// Init var
		$result = (
			(is_null($warnHandlerCollection)) ||
			($warnHandlerCollection instanceof WarnHandlerCollectionInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($warnHandlerCollection);
		}
		
		// Return result
		return $result;
    }
	
	
	
}