<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\tryer\exception;

use liberty_code\error\warning\tryer\library\ConstWarnTryer;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstWarnTryer::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid search cache only required option
            (
                (!isset($config[ConstWarnTryer::TAB_CONFIG_KEY_WARN_EXECUTION_RETURN_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstWarnTryer::TAB_CONFIG_KEY_WARN_EXECUTION_RETURN_REQUIRE]) ||
                    is_int($config[ConstWarnTryer::TAB_CONFIG_KEY_WARN_EXECUTION_RETURN_REQUIRE]) ||
                    (
                        is_string($config[ConstWarnTryer::TAB_CONFIG_KEY_WARN_EXECUTION_RETURN_REQUIRE]) &&
                        ctype_digit($config[ConstWarnTryer::TAB_CONFIG_KEY_WARN_EXECUTION_RETURN_REQUIRE])
                    )
                )
            ) &&

            // Check valid select warning execution return option
            (
                (!isset($config[ConstWarnTryer::TAB_CONFIG_KEY_SELECT_WARN_EXECUTION_RETURN])) ||
                (
                    // Check is valid option
                    is_string($config[ConstWarnTryer::TAB_CONFIG_KEY_SELECT_WARN_EXECUTION_RETURN]) &&
                    (
                        ($config[ConstWarnTryer::TAB_CONFIG_KEY_SELECT_WARN_EXECUTION_RETURN] ==
                            ConstWarnTryer::CONFIG_SELECT_WARN_EXECUTION_RETURN_FIRST) ||
                        ($config[ConstWarnTryer::TAB_CONFIG_KEY_SELECT_WARN_EXECUTION_RETURN] ==
                            ConstWarnTryer::CONFIG_SELECT_WARN_EXECUTION_RETURN_LAST)
                    )
                )
            ) &&

            // Check valid warning register clear required option
            (
                (!isset($config[ConstWarnTryer::TAB_CONFIG_KEY_WARN_REGISTER_CLEAR_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstWarnTryer::TAB_CONFIG_KEY_WARN_REGISTER_CLEAR_REQUIRE]) ||
                    is_int($config[ConstWarnTryer::TAB_CONFIG_KEY_WARN_REGISTER_CLEAR_REQUIRE]) ||
                    (
                        is_string($config[ConstWarnTryer::TAB_CONFIG_KEY_WARN_REGISTER_CLEAR_REQUIRE]) &&
                        ctype_digit($config[ConstWarnTryer::TAB_CONFIG_KEY_WARN_REGISTER_CLEAR_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}