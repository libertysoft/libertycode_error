<?php
/**
 * Description :
 * This class allows to define default warning tryer class.
 *
 * Default warning tryer uses the following specified configuration:
 * [
 *     Default tryer configuration,
 *
 *     warn_execution_return_require(optional: got true if not found): true / false,
 *
 *     select_warn_execution_return(optional: got last if not found): 'first' / 'last',
 *
 *     warn_register_clear_require(optional: got false if not found): true / false
 * ]
 *
 * Note:
 * -> Configuration warn_execution_return_require:
 * Used on warnings action execution.
 * If true, allows to use previous warning execution return, instead of process return,
 * to get current warning execution return.
 * If false or if it's first warning execution, use process return.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\warning\tryer\model;

use liberty_code\error\tryer\model\DefaultTryer;
use liberty_code\error\warning\tryer\api\WarnTryerInterface;

use Throwable;
use liberty_code\register\register\api\RegisterInterface;
use liberty_code\error\handler\api\HandlerCollectionInterface;
use liberty_code\error\tryer\library\ConstTryer;
use liberty_code\error\warning\library\ToolBoxWarnRegister;
use liberty_code\error\warning\handler\api\WarnHandlerCollectionInterface;
use liberty_code\error\warning\tryer\library\ConstWarnTryer;
use liberty_code\error\warning\tryer\exception\WarnHandlerCollectionInvalidFormatException;
use liberty_code\error\warning\tryer\exception\WarnRegisterInvalidFormatException;
use liberty_code\error\warning\tryer\exception\ConfigInvalidFormatException;



/**
 * @method RegisterInterface getObjWarnRegister() Get warning register object.
 * @method void setObjWarnRegister(RegisterInterface $objWarnRegister) Set warning register object.
 */
class DefaultWarnTryer extends DefaultTryer implements WarnTryerInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param WarnHandlerCollectionInterface $objWarnHandlerCollection
     * @param RegisterInterface $objWarnRegister
     */
    public function __construct(
        HandlerCollectionInterface $objHandlerCollection,
        WarnHandlerCollectionInterface $objWarnHandlerCollection,
        RegisterInterface $objWarnRegister,
        array $tabConfig = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objHandlerCollection,
            $tabConfig
        );

        // Init warning handler collection
        $this->setWarnHandlerCollection($objWarnHandlerCollection);

        // Init warning register
        $this->setObjWarnRegister($objWarnRegister);
    }



	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
		if(!$this->beanExists(ConstWarnTryer::DATA_KEY_DEFAULT_WARN_HANDLER_COLLECTION))
        {
            $this->__beanTabData[ConstWarnTryer::DATA_KEY_DEFAULT_WARN_HANDLER_COLLECTION] = null;
        }

        if(!$this->beanExists(ConstWarnTryer::DATA_KEY_DEFAULT_WARN_REGISTER))
        {
            $this->__beanTabData[ConstWarnTryer::DATA_KEY_DEFAULT_WARN_REGISTER] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
            ConstWarnTryer::DATA_KEY_DEFAULT_WARN_HANDLER_COLLECTION,
            ConstWarnTryer::DATA_KEY_DEFAULT_WARN_REGISTER
		);
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstWarnTryer::DATA_KEY_DEFAULT_WARN_HANDLER_COLLECTION:
					WarnHandlerCollectionInvalidFormatException::setCheck($value);
					break;

                case ConstWarnTryer::DATA_KEY_DEFAULT_WARN_REGISTER:
                    WarnRegisterInvalidFormatException::setCheck($value);
                    break;

                case ConstTryer::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}





    // Methods check
    // ******************************************************************************

    /**
     * Check warning execution return option required,
     * for warnings action execution.
     *
     * @return boolean
     */
    public function checkWarnExecutionReturnRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstWarnTryer::TAB_CONFIG_KEY_WARN_EXECUTION_RETURN_REQUIRE])) ||
            (intval($tabConfig[ConstWarnTryer::TAB_CONFIG_KEY_WARN_EXECUTION_RETURN_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check select warning execution return first option required,
     * for warnings action execution.
     *
     * @return boolean
     */
    public function checkSelectWarnExecutionReturnFirstRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstWarnTryer::TAB_CONFIG_KEY_SELECT_WARN_EXECUTION_RETURN]) &&
            ($tabConfig[ConstWarnTryer::TAB_CONFIG_KEY_SELECT_WARN_EXECUTION_RETURN] ==
                ConstWarnTryer::CONFIG_SELECT_WARN_EXECUTION_RETURN_FIRST)
        );

        // Return result
        return $result;
    }



    /**
     * Check select warning execution return last option required,
     * for warnings action execution.
     *
     * @return boolean
     */
    public function checkSelectWarnExecutionReturnLastRequired()
    {
        // Return result
        return (!$this->checkSelectWarnExecutionReturnFirstRequired());
    }



    /**
     * Check warning register clear option required,
     * after warnings action execution.
     *
     * @return boolean
     */
    public function checkWarnRegisterClearRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstWarnTryer::TAB_CONFIG_KEY_WARN_REGISTER_CLEAR_REQUIRE]) &&
            (intval($tabConfig[ConstWarnTryer::TAB_CONFIG_KEY_WARN_REGISTER_CLEAR_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

	/**
     * @inheritdoc
     */
    public function getObjWarnHandlerCollection()
    {
        // Return result
        return $this->beanGet(ConstWarnTryer::DATA_KEY_DEFAULT_WARN_HANDLER_COLLECTION);
    }

	



    // Methods setters
    // ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function setWarnHandlerCollection(WarnHandlerCollectionInterface $objWarnHandlerCollection)
	{
		$this->beanSet(ConstWarnTryer::DATA_KEY_DEFAULT_WARN_HANDLER_COLLECTION, $objWarnHandlerCollection);
	}



    /**
     * Put specified throwable,
     * on warning register.
     * Return string register key if success, false else.
     *
     * Configuration format:
     * @see ToolBoxWarnRegister::putThrowable() configuration format.
     *
     * @param Throwable $objThrowable
     * @param string $strKey = null
     * @param array $tabConfig = null
     * @return boolean|string
     */
    public function putWarnThrowable(
        Throwable $objThrowable,
        $strKey = null,
        array $tabConfig = null
    )
    {
        // Init var
        $objWarnRegister = $this->getObjWarnRegister();

        // Return result
        return ToolBoxWarnRegister::putThrowable(
            $objWarnRegister,
            $objThrowable,
            $strKey,
            $tabConfig
        );
    }



    /**
     * Remove specified throwable,
     * from warning register.
     *
     * @param string|Throwable $throwable : string throwable key|throwable object
     * @return boolean
     */
    public function removeWarnThrowable($throwable)
    {
        // Init var
        $objWarnRegister = $this->getObjWarnRegister();

        // Return result
        return ToolBoxWarnRegister::removeThrowable(
            $objWarnRegister,
            $throwable
        );
    }




	
	// Methods execute
    // ******************************************************************************

    /**
     * Execute action,
     * from warnings on warning register.
     *
     * @param null|mixed $resultExecution
     * @param boolean $boolExecutionSuccess
     * @return null|mixed
     */
    protected function executeWarning($resultExecution, $boolExecutionSuccess)
    {
        // Init var
        $result = $resultExecution;
        $objWarnRegister = $this->getObjWarnRegister();
        $tabWarning = array_values($objWarnRegister->getTabItem());
        $objWarnHandlerCollection = $this->getObjWarnHandlerCollection();
        $tabWarnHandlerWarning = array();
        $tabWarnHandler = $objWarnHandlerCollection->getTabWarnHandler($tabWarning, $tabWarnHandlerWarning);

        // Execute warnings action, if required
        if(count($tabWarnHandler) > 0)
        {
            // Run each warning handler and execute action
            $tabResult = array();
            $intCountResult = 0;
            $boolWarnExecutionReturn = $this->checkWarnExecutionReturnRequired();
            foreach($tabWarnHandler as $strKey => $objWarnHandler)
            {
                $tabCurrentWarnHandlerWarning = $tabWarnHandlerWarning[$strKey];
                $tabResult[] = $objWarnHandler->execute(
                    $tabCurrentWarnHandlerWarning,
                    (
                        (
                            $boolWarnExecutionReturn &&
                            ($intCountResult > 0)
                        ) ?
                            $tabResult[($intCountResult - 1)] :
                            $resultExecution
                    ),
                    $boolExecutionSuccess
                );

                $intCountResult++;
            }

            // Select result
            $result = (
                $this->checkSelectWarnExecutionReturnFirstRequired() ?
                    $tabResult[0] :
                    $tabResult[($intCountResult - 1)]
            );
        }

        // Clear warning register, if required
        if($this->checkWarnRegisterClearRequired())
        {
            $objWarnRegister->removeItemAll();
        }

        // Return result
        return $result;
    }



	/**
	 * @inheritdoc
	 */
	public function execute(callable $callableExecution, &$boolExecutionSuccess = true)
	{
		// Init var
		$result = parent::execute($callableExecution, $boolExecutionSuccess);
        $result = $this->executeWarning($result, $boolExecutionSuccess); // Execute action from warnings
		
		// Return result
		return $result;
	}
	
	
	
}