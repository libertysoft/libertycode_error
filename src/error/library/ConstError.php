<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\error\library;



class ConstError
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_ERROR_CODE = 'error_code';
    const TAB_CONFIG_KEY_ERROR_TYPE = 'error_type';
    const TAB_CONFIG_KEY_ERROR_MESSAGE = 'error_message';
    const TAB_CONFIG_KEY_FILE_PATH = 'file_path';
    const TAB_CONFIG_KEY_FILE_LINE_NUM = 'file_line_num';
}