<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\error\library;

use ErrorException;
use liberty_code\error\error\library\ConstError;



class ToolBoxErrorExceptionFactory
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified configuration array,
     * used to get error exception,
     * has valid format.
     *
     * Configuration array format:
     * @see getObjErrorException() configuration array format.
     *
     * @param array $tabConfig
     * @return boolean
     */
    public static function checkConfigIsValid(array $tabConfig)
    {
        // Init var
        $result =
            // Check valid error code
            (
                (!isset($tabConfig[ConstError::TAB_CONFIG_KEY_ERROR_CODE])) ||
                (
                    is_int($tabConfig[ConstError::TAB_CONFIG_KEY_ERROR_CODE]) &&
                    (($tabConfig[ConstError::TAB_CONFIG_KEY_ERROR_CODE]) >= 0)
                )
            ) &&

            // Check valid error level
            isset($tabConfig[ConstError::TAB_CONFIG_KEY_ERROR_TYPE]) &&
            is_int($tabConfig[ConstError::TAB_CONFIG_KEY_ERROR_TYPE]) &&
            (($tabConfig[ConstError::TAB_CONFIG_KEY_ERROR_TYPE]) > 0) &&

            // Check valid error message
            isset($tabConfig[ConstError::TAB_CONFIG_KEY_ERROR_MESSAGE]) &&
            is_string($tabConfig[ConstError::TAB_CONFIG_KEY_ERROR_MESSAGE]) &&
            (trim($tabConfig[ConstError::TAB_CONFIG_KEY_ERROR_MESSAGE]) != '') &&

            // Check valid file path
            (
                (!isset($tabConfig[ConstError::TAB_CONFIG_KEY_FILE_PATH])) ||
                (
                    is_string($tabConfig[ConstError::TAB_CONFIG_KEY_FILE_PATH]) &&
                    (trim($tabConfig[ConstError::TAB_CONFIG_KEY_FILE_PATH]) != '') &&
                    is_file($tabConfig[ConstError::TAB_CONFIG_KEY_FILE_PATH])
                )
            ) &&

            // Check valid file line number
            (
                (!isset($tabConfig[ConstError::TAB_CONFIG_KEY_FILE_LINE_NUM])) ||
                (
                    is_int($tabConfig[ConstError::TAB_CONFIG_KEY_FILE_LINE_NUM]) &&
                    (($tabConfig[ConstError::TAB_CONFIG_KEY_FILE_LINE_NUM]) > 0)
                )
            );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get configuration array,
     * used to get error exception,
     * from specified error information.
     * Return null, if configuration failed.
     *
     * Return array format:
     * @see checkConfigIsValid() configuration array format.
     *
     * @param integer $intErrType
     * @param string $strErrMsg
     * @param integer $intErrCode = null
     * @param string $strFilePath = null
     * @param integer $intFileLine = null
     * @return null|array
     */
    public static function getTabConfig(
        $intErrType,
        $strErrMsg,
        $intErrCode = null,
        $strFilePath = null,
        $intFileLine = null
    )
    {
        // Init configuration array
        $tabConfig = array(
            ConstError::TAB_CONFIG_KEY_ERROR_TYPE => $intErrType,
            ConstError::TAB_CONFIG_KEY_ERROR_MESSAGE => $strErrMsg
        );
        // Set error code, if required
        if(!is_null($intErrCode)) $tabConfig[ConstError::TAB_CONFIG_KEY_ERROR_CODE] = $intErrCode;
        // Set file path, if required
        if(!is_null($strFilePath)) $tabConfig[ConstError::TAB_CONFIG_KEY_FILE_PATH] = $strFilePath;
        // Set file line, if required
        if(!is_null($intFileLine)) $tabConfig[ConstError::TAB_CONFIG_KEY_FILE_LINE_NUM] = $intFileLine;

        // Get result
        $result = (
            static::checkConfigIsValid($tabConfig) ?
                $tabConfig :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get new error exception object,
     * from specified configuration.
     * Return null, if configuration failed.
     *
     * Configuration array format:
     * {
     *     error_code(optional: got 0, if not found): Integer error code,
     *
     *     error_type(required): Integer error level,
     *
     *     error_message(required): "String error message",
     *
     *     file_name(optional: got current file path (__FILE__), if not found): "String file path",
     *
     *     file_line_num(optional: got current file line (__LINE__), if not found): Integer file line number
     * }
     *
     * @param array $tabConfig
     * @return null|ErrorException
     */
    public static function getObjErrorException(array $tabConfig)
    {
        // Init var
        $result = null;

        // Get error exception, if required
        if(static::checkConfigIsValid($tabConfig))
        {
            // Get info
            $intErrCode = (
                (isset($tabConfig[ConstError::TAB_CONFIG_KEY_ERROR_CODE])) ?
                    $tabConfig[ConstError::TAB_CONFIG_KEY_ERROR_CODE] :
                    0
            );
            $intErrType = $tabConfig[ConstError::TAB_CONFIG_KEY_ERROR_TYPE];
            $strErrMsg = $tabConfig[ConstError::TAB_CONFIG_KEY_ERROR_MESSAGE];
            $strFilePath = (
                (isset($tabConfig[ConstError::TAB_CONFIG_KEY_FILE_PATH])) ?
                    $tabConfig[ConstError::TAB_CONFIG_KEY_FILE_PATH] :
                    __FILE__
            );
            $intFileLine =  (
                (isset($tabConfig[ConstError::TAB_CONFIG_KEY_FILE_LINE_NUM])) ?
                    $tabConfig[ConstError::TAB_CONFIG_KEY_FILE_LINE_NUM] :
                    __LINE__
            );

            // Get error exception
            $result = new ErrorException(
                $strErrMsg,
                $intErrCode,
                $intErrType,
                $strFilePath,
                $intFileLine
            );
        }

        // Return result
        return $result;
    }



}