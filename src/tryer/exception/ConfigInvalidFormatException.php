<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\tryer\exception;

use liberty_code\error\tryer\library\ConstTryer;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstTryer::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid select throwable execution return option
            (
                (!isset($config[ConstTryer::TAB_CONFIG_KEY_SELECT_THROW_EXECUTION_RETURN])) ||
                (
                    // Check is valid option
                    is_string($config[ConstTryer::TAB_CONFIG_KEY_SELECT_THROW_EXECUTION_RETURN]) &&
                    (
                        ($config[ConstTryer::TAB_CONFIG_KEY_SELECT_THROW_EXECUTION_RETURN] ==
                            ConstTryer::CONFIG_SELECT_THROW_EXECUTION_RETURN_FIRST) ||
                        ($config[ConstTryer::TAB_CONFIG_KEY_SELECT_THROW_EXECUTION_RETURN] ==
                            ConstTryer::CONFIG_SELECT_THROW_EXECUTION_RETURN_LAST)
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}