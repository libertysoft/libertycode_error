<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\tryer\exception;

use liberty_code\error\handler\api\HandlerCollectionInterface;
use liberty_code\error\tryer\library\ConstTryer;



class HandlerCollectionInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $handlerCollection
     */
	public function __construct($handlerCollection)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstTryer::EXCEPT_MSG_HANDLER_COLLECTION_INVALID_FORMAT,
            mb_strimwidth(strval($handlerCollection), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified handler collection has valid format.
	 * 
     * @param mixed $handlerCollection
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($handlerCollection)
    {
		// Init var
		$result = (
			(is_null($handlerCollection)) ||
			($handlerCollection instanceof HandlerCollectionInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($handlerCollection);
		}
		
		// Return result
		return $result;
    }
	
	
	
}