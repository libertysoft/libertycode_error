<?php
/**
 * Description :
 * This class allows to define default tryer class.
 *
 * Default tryer uses the following specified configuration:
 * [
 *     select_throw_execution_return(optional: got first if not found): 'first' / 'last'
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\tryer\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\error\tryer\api\TryerInterface;

use Throwable;
use liberty_code\error\error\library\ToolBoxErrorExceptionFactory;
use liberty_code\error\handler\api\HandlerCollectionInterface;
use liberty_code\error\tryer\library\ConstTryer;
use liberty_code\error\tryer\exception\HandlerCollectionInvalidFormatException;
use liberty_code\error\tryer\exception\ConfigInvalidFormatException;



/**
 * @method array getTabConfig() Get configuration array.
 * @method void setTabConfig(array $tabConfig) Set configuration array.
 */
class DefaultTryer extends FixBean implements TryerInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param HandlerCollectionInterface $objHandlerCollection
     * @param array $tabConfig = null
     */
    public function __construct(
        HandlerCollectionInterface $objHandlerCollection,
        array $tabConfig = null
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init handler collection
        $this->setHandlerCollection($objHandlerCollection);

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }
    }



	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
		if(!$this->beanExists(ConstTryer::DATA_KEY_DEFAULT_HANDLER_COLLECTION))
        {
            $this->__beanTabData[ConstTryer::DATA_KEY_DEFAULT_HANDLER_COLLECTION] = null;
        }

        if(!$this->beanExists(ConstTryer::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstTryer::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
            ConstTryer::DATA_KEY_DEFAULT_HANDLER_COLLECTION,
            ConstTryer::DATA_KEY_DEFAULT_CONFIG
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstTryer::DATA_KEY_DEFAULT_HANDLER_COLLECTION:
					HandlerCollectionInvalidFormatException::setCheck($value);
					break;

                case ConstTryer::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}





    // Methods check
    // ******************************************************************************

    /**
     * Check select throwable execution return first option required,
     * for throwable action execution.
     *
     * @return boolean
     */
    public function checkSelectThrowExecutionReturnFirstRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstTryer::TAB_CONFIG_KEY_SELECT_THROW_EXECUTION_RETURN])) ||
            ($tabConfig[ConstTryer::TAB_CONFIG_KEY_SELECT_THROW_EXECUTION_RETURN] ==
                ConstTryer::CONFIG_SELECT_THROW_EXECUTION_RETURN_FIRST)
        );

        // Return result
        return $result;
    }



    /**
     * Check select throwable execution return last option required,
     * for throwable action execution.
     *
     * @return boolean
     */
    public function checkSelectThrowExecutionReturnLastRequired()
    {
        // Return result
        return (!$this->checkSelectThrowExecutionReturnFirstRequired());
    }





    // Methods getters
    // ******************************************************************************

	/**
     * @inheritdoc
     */
    public function getObjHandlerCollection()
    {
        // Return result
        return $this->beanGet(ConstTryer::DATA_KEY_DEFAULT_HANDLER_COLLECTION);
    }

	



    // Methods setters
    // ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function setHandlerCollection(HandlerCollectionInterface $objHandlerCollection)
	{
		$this->beanSet(ConstTryer::DATA_KEY_DEFAULT_HANDLER_COLLECTION, $objHandlerCollection);
	}


	

	
	// Methods execute
    // ******************************************************************************

    /**
     * Execute action,
     * from specified standard error.
     * Return true if specified error handle, false else.
     *
     * @param integer $intErrType
     * @param string $strErrMsg
     * @param string $strFilePath
     * @param integer $intFileLine
     * @return boolean
     */
    protected function executeError(
        $intErrType,
        $strErrMsg,
        $strFilePath,
        $intFileLine
    )
    {
        // Init var
        $error = ToolBoxErrorExceptionFactory::getTabConfig(
            $intErrType,
            $strErrMsg,
            null,
            $strFilePath,
            $intFileLine
        );
        $objHandlerCollection = $this->getObjHandlerCollection();
        $tabHandler = $objHandlerCollection->getTabHandler($error);

        // Run each handler and execute action
        foreach($tabHandler as $objHandler)
        {
            $objHandler->execute($error);
        }

        // Return result
        return (count($tabHandler) > 0);
    }



    /**
     * Execute action,
     * from specified throwable.
     * Throw specified throwable, if not handle.
     *
     * @param Throwable $objThrowable
     * @return null|mixed
     * @throws Throwable
     */
    protected function executeThrowable(Throwable $objThrowable)
    {
        // Init var
        $objHandlerCollection = $this->getObjHandlerCollection();
        $tabHandler = $objHandlerCollection->getTabHandler($objThrowable);

        // Check valid array of handlers
        if(count($tabHandler) == 0)
        {
            throw $objThrowable;
        }

        // Run each handler and execute action
        $tabResult = array();
        foreach($tabHandler as $objHandler)
        {
            try{
                $tabResult[] = $objHandler->execute($objThrowable);
            }
            catch(Throwable $objSubThrowable)
            {
                $tabResult[] = $this->executeThrowable($objSubThrowable);
            }
        }

        // Select result
        $result = (
            $this->checkSelectThrowExecutionReturnFirstRequired() ?
                $tabResult[0] :
                $tabResult[(count($tabResult) - 1)]
        );

        // Return result
        return $result;
    }



	/**
	 * @inheritdoc
	 */
	public function execute(callable $callableExecution, &$boolExecutionSuccess = true)
	{
		// Init var
		$result = null;
        $boolExecutionSuccess = false;

		// Try to execute process
		try
        {
            // Register action execution from standard error
            set_error_handler(
                function($intErrType, $strErrMsg, $strFilePath, $intFileLine)
                {
                    return $this->executeError($intErrType, $strErrMsg, $strFilePath, $intFileLine);
                }
            );

            // Execute process
            $result = $callableExecution();
            $boolExecutionSuccess = true;
        }
        // Execute action from catch throwable, if required
        catch(Throwable $objThrowable)
        {
            $result = $this->executeThrowable($objThrowable);
        }
        finally
        {
            // Reset error handling
            restore_error_handler();
        }
		
		// Return result
		return $result;
	}
	
	
	
}