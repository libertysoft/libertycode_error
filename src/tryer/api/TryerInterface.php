<?php
/**
 * Description :
 * This class allows to describe behavior of tryer class.
 * Tryer allows to try execution of specific process
 * and handle potential failures (errors) when occurs,
 * using handler collection.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\tryer\api;

use liberty_code\error\handler\api\HandlerCollectionInterface;



interface TryerInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get handler collection object.
     *
     * @return HandlerCollectionInterface
     */
    public function getObjHandlerCollection();





    // Methods setters
    // ******************************************************************************

    /**
     * Set handler collection object.
     *
     * @param HandlerCollectionInterface $objHandlerCollection
     */
    public function setHandlerCollection(HandlerCollectionInterface $objHandlerCollection);





    // Methods execute
    // ******************************************************************************

    /**
     * Execute specified process.
     * Execution callable format: mixed function().
     *
     * @param callable $callableExecution
     * @param boolean &$boolExecutionSuccess = true
     * @return null|mixed
     */
    public function execute(callable $callableExecution, &$boolExecutionSuccess = true);
}