<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/build/test/HandlerBuilderTest.php');

// Use
use liberty_code\error\handler\model\DefaultHandlerCollection;
use liberty_code\error\tryer\model\DefaultTryer;



// Init var
$objHandlerCollection = new DefaultHandlerCollection();
$objTryer = new DefaultTryer($objHandlerCollection);



// Hydrate handler collection
$tabDataSrc = array(
    $strRootAppPath . '/src/handler/test/handler'
);

$objHandlerDirBuilder->setTabDataSrc($tabDataSrc);
$objHandlerDirBuilder->hydrateHandlerCollection($objHandlerCollection);


