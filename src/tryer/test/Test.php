<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/handler/test/TestException.php');
require_once($strRootAppPath . '/src/tryer/test/TryerTest.php');

// Use test
use liberty_code\error\handler\test\TestException;



// Test tryer execution
$tabCallableExecution = array(
    function(){
        trigger_error('User warning test', E_USER_WARNING);
        trigger_error('User notice test', E_USER_NOTICE);
        trigger_error('User error test', E_USER_ERROR);

        return 'Execution done';
    }, // Ko: Error handle

    function(){
        trigger_error('User warning test', E_USER_WARNING);
        trigger_error('User notice test', E_USER_NOTICE);

        return 'Execution done';
    }, // Ok

    function(){
        trigger_error('User warning test', E_USER_WARNING);
        trigger_error('User notice test', E_USER_NOTICE);

        throw new Exception(
            'Exception 1 test',
            400
        );

        return 'Execution done';
    }, // Ko: Throwable handle

    function(){
        return 'Execution done';
    }, // Ok

    function(){
        throw new Exception(
            'Exception 2 test',
            500
        );

        return 'Execution done';
    }, // Ko: Throwable handle

    function(){
        throw new TestException();

        return 'Execution done';
    } // Ko: Test exception handle -> Throwable handle
);

foreach($tabCallableExecution as $callableExecution)
{
    echo('Test tryer execution: <br />');

	try{
        $boolExecutionSuccess = false;
        $resultExecution = $objTryer->execute($callableExecution, $boolExecutionSuccess);

        echo('Execution result: <pre>');print_r($resultExecution);echo('</pre>');
        echo('Execution success: <pre>');var_dump($boolExecutionSuccess);echo('</pre>');

	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



trigger_error('User error test', E_USER_ERROR);


