<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\error\tryer\library;



class ConstTryer
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
	const DATA_KEY_DEFAULT_HANDLER_COLLECTION = 'objHandlerCollection';
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';



    // Configuration
    const TAB_CONFIG_KEY_SELECT_THROW_EXECUTION_RETURN = 'select_throw_execution_return';

    // Throwable execution return selection configuration
    const CONFIG_SELECT_THROW_EXECUTION_RETURN_FIRST = 'first';
    const CONFIG_SELECT_THROW_EXECUTION_RETURN_LAST = 'last';



    // Exception message constants
    const EXCEPT_MSG_HANDLER_COLLECTION_INVALID_FORMAT =
        'Following handler collection "%1$s" invalid! It must be a handler collection object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default tryer configuration standard.';
}