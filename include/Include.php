<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/error/library/ConstError.php');
include($strRootPath . '/src/error/library/ToolBoxErrorExceptionFactory.php');

include($strRootPath . '/src/handler/library/ConstHandler.php');
include($strRootPath . '/src/handler/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/handler/exception/CollectionConfigInvalidFormatException.php');
include($strRootPath . '/src/handler/exception/CollectionKeyInvalidFormatException.php');
include($strRootPath . '/src/handler/exception/CollectionValueInvalidFormatException.php');
include($strRootPath . '/src/handler/api/HandlerInterface.php');
include($strRootPath . '/src/handler/api/HandlerCollectionInterface.php');
include($strRootPath . '/src/handler/model/DefaultHandler.php');
include($strRootPath . '/src/handler/model/DefaultHandlerCollection.php');

include($strRootPath . '/src/handler/error/library/ConstErrorHandler.php');
include($strRootPath . '/src/handler/error/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/handler/error/model/ErrorHandler.php');
include($strRootPath . '/src/handler/error/model/ThrowErrorHandler.php');

include($strRootPath . '/src/handler/error/fix/library/ConstFixErrorHandler.php');
include($strRootPath . '/src/handler/error/fix/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/handler/error/fix/model/FixErrorHandler.php');
include($strRootPath . '/src/handler/error/fix/model/FixThrowErrorHandler.php');

include($strRootPath . '/src/handler/throwable/library/ConstThrowableHandler.php');
include($strRootPath . '/src/handler/throwable/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/handler/throwable/model/ThrowableHandler.php');

include($strRootPath . '/src/handler/throwable/fix/library/ConstFixThrowableHandler.php');
include($strRootPath . '/src/handler/throwable/fix/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/handler/throwable/fix/model/FixThrowableHandler.php');

include($strRootPath . '/src/handler/factory/library/ConstHandlerFactory.php');
include($strRootPath . '/src/handler/factory/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/handler/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/handler/factory/api/HandlerFactoryInterface.php');
include($strRootPath . '/src/handler/factory/model/DefaultHandlerFactory.php');

include($strRootPath . '/src/handler/factory/standard/library/ConstStandardHandlerFactory.php');
include($strRootPath . '/src/handler/factory/standard/model/StandardHandlerFactory.php');

include($strRootPath . '/src/build/library/ConstBuilder.php');
include($strRootPath . '/src/build/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/build/exception/DataSrcInvalidFormatException.php');
include($strRootPath . '/src/build/api/BuilderInterface.php');
include($strRootPath . '/src/build/model/DefaultBuilder.php');

include($strRootPath . '/src/build/directory/library/ConstDirBuilder.php');
include($strRootPath . '/src/build/directory/exception/DataSrcInvalidFormatException.php');
include($strRootPath . '/src/build/directory/model/DirBuilder.php');

include($strRootPath . '/src/tryer/library/ConstTryer.php');
include($strRootPath . '/src/tryer/exception/HandlerCollectionInvalidFormatException.php');
include($strRootPath . '/src/tryer/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/tryer/api/TryerInterface.php');
include($strRootPath . '/src/tryer/model/DefaultTryer.php');

include($strRootPath . '/src/warning/library/ConstWarning.php');
include($strRootPath . '/src/warning/library/ToolBoxWarnRegister.php');

include($strRootPath . '/src/warning/handler/library/ConstWarnHandler.php');
include($strRootPath . '/src/warning/handler/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/warning/handler/exception/CollectionConfigInvalidFormatException.php');
include($strRootPath . '/src/warning/handler/exception/CollectionKeyInvalidFormatException.php');
include($strRootPath . '/src/warning/handler/exception/CollectionValueInvalidFormatException.php');
include($strRootPath . '/src/warning/handler/api/WarnHandlerInterface.php');
include($strRootPath . '/src/warning/handler/api/WarnHandlerCollectionInterface.php');
include($strRootPath . '/src/warning/handler/model/DefaultWarnHandler.php');
include($strRootPath . '/src/warning/handler/model/DefaultWarnHandlerCollection.php');

include($strRootPath . '/src/warning/handler/throwable/library/ConstThrowableWarnHandler.php');
include($strRootPath . '/src/warning/handler/throwable/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/warning/handler/throwable/model/ThrowableWarnHandler.php');

include($strRootPath . '/src/warning/handler/throwable/fix/library/ConstFixThrowableWarnHandler.php');
include($strRootPath . '/src/warning/handler/throwable/fix/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/warning/handler/throwable/fix/model/FixThrowableWarnHandler.php');

include($strRootPath . '/src/warning/handler/factory/library/ConstWarnHandlerFactory.php');
include($strRootPath . '/src/warning/handler/factory/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/warning/handler/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/warning/handler/factory/api/WarnHandlerFactoryInterface.php');
include($strRootPath . '/src/warning/handler/factory/model/DefaultWarnHandlerFactory.php');

include($strRootPath . '/src/warning/build/library/ConstBuilder.php');
include($strRootPath . '/src/warning/build/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/warning/build/exception/DataSrcInvalidFormatException.php');
include($strRootPath . '/src/warning/build/api/BuilderInterface.php');
include($strRootPath . '/src/warning/build/model/DefaultBuilder.php');

include($strRootPath . '/src/warning/build/directory/library/ConstDirBuilder.php');
include($strRootPath . '/src/warning/build/directory/exception/DataSrcInvalidFormatException.php');
include($strRootPath . '/src/warning/build/directory/model/DirBuilder.php');

include($strRootPath . '/src/warning/tryer/library/ConstWarnTryer.php');
include($strRootPath . '/src/warning/tryer/exception/WarnHandlerCollectionInvalidFormatException.php');
include($strRootPath . '/src/warning/tryer/exception/WarnRegisterInvalidFormatException.php');
include($strRootPath . '/src/warning/tryer/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/warning/tryer/api/WarnTryerInterface.php');
include($strRootPath . '/src/warning/tryer/model/DefaultWarnTryer.php');

include($strRootPath . '/src/warning/error/handler/error/library/ConstWarnErrorHandler.php');
include($strRootPath . '/src/warning/error/handler/error/model/WarnErrorHandler.php');

include($strRootPath . '/src/warning/error/handler/error/fix/model/FixWarnErrorHandler.php');

include($strRootPath . '/src/warning/error/handler/factory/library/ConstWarnHandlerFactory.php');
include($strRootPath . '/src/warning/error/handler/factory/model/WarnHandlerFactory.php');