LibertyCode_Error
=================



Description
-----------

Library contains error handling components, 
to manage application errors.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/error ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/error": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### Handler

Handlers allows to handle specific errors,
to execute specific action.

_Elements_

- Handler

    Allows to design a handler, 
    which contains all information, 
    to handle specific errors.
    to execute specific action.

- ErrorHandler

    Extends handler features. 
    Allows to handle standard errors,
    with error exception.

- ThrowErrorHandler

    Extends error handler features. 
    Execute action by throwing error exception.
    
- FixErrorHandler

    Extends error handler features. 
    Allows to handle standard errors,
    from a specified fixed configuration.

- FixThrowErrorHandler

    Extends throw error handler features. 
    Allows to throw error exception,
    from a specified fixed configuration.
    
- ThrowableHandler

    Extends handler features. 
    Allows to handle throwable errors.
    
- FixThrowableHandler

    Extends throwable handler features. 
    Allows to handle throwable errors,
    from a specified fixed configuration.
    
- HandlerCollection

    Allows to design collection of handlers.
    Allows to get list of handles,
    from a specific error.
    
- HandlerFactory

    Allows to design a handler factory, 
    to provide new or specified handler instance,
    from specified configuration or file path.
    
- StandardHandlerFactory

    Extends handler factory features. 
    Provides handler instance.
    
_Example_

```php
// Get handler factory
use liberty_code\error\handler\factory\model\DefaultHandlerFactory;
$handlerFactory = new DefaultHandlerFactory(null, $provider);
...
// Get new handler from configuration
$handler = $handlerFactory->getObjHandler(array(...));
...
// Get new handler from handler class file path
$handler = $handlerFactory->getObjHandlerFromFile('...');
...
```

#### Builder

Builder allows to hydrate handler collection, with handlers.

_Elements_

- Builder
    
    Uses array of source data to hydrate handler collection.

- DirBuilder
    
    Extends builder features. 
    Uses array of directories paths, 
    to hydrate handler collection,
    from each file path, on each directory path.
    
_Example_

```php
// Get handler collection
use liberty_code\error\handler\model\DefaultHandlerCollection;
$handlerCollection = new DefaultHandlerCollection();
...
// Get handler builder
use liberty_code\error\build\model\DefaultBuilder;
$handlerBuilder = new DefaultBuilder($handlerFactory);
...
// Hydrate handler collection
$handlerBuilder->setTabDataSrc(array(...));
$handlerBuilder->hydrateHandlerCollection($handlerCollection);
...
// Get handler directory builder
use liberty_code\error\build\directory\model\DirBuilder;
$handlerDirBuilder = new DirBuilder($handlerFactory);
...
// Hydrate handler collection
$handlerDirBuilder->setTabDataSrc(array(...));
$handlerDirBuilder->hydrateHandlerCollection($handlerCollection, false);
...
foreach($handlerCollection->getTabKey() as $key) {
    echo($handlerCollection->getObjHandler($key)->getStrKey() .'<br />');
}
/**
 * Show: 
 * Handler key 1
 * ...
 * Handler key N
 */
...
```

#### Tryer

Tryer allows to execute specific process and handle potential failures when occurs,
using handler collection.

_Elements_

- Tryer

    Allows to design a tryer,
    which try execution of specific process
    and handle potential failures when occurs,
    using handler collection.
    
_Example_

```php
use liberty_code\error\tryer\model\DefaultTryer;
$tryer = new DefaultTryer($handlerCollection);
...
// Try specific process execution
$executionSuccess = false;
$resultExecution = $tryer->execute(callable..., $executionSuccess);
...
// Show if specific process succeeded or failed
var_dump($executionSuccess);
...
// Show return from specific process if success, from error handling else
var_dump($resultExecution);
...
```

#### Warning handler

Warning handlers allows to handle specific warnings,
to execute specific action, 
if main process succeeded or failed.

_Elements_

- WarnHandler

    which contains all information, 
    to handle specific warnings,
    to execute specific action,
    if main process succeeded or failed.

- ThrowableWarnHandler

    Extends warning handler features. 
    Allows to handle throwable warnings.
    
- FixThrowableHandler

    Extends throwable warning handler features. 
    Allows to handle throwable warnings,
    from a specified fixed configuration.
    
- WarnHandlerCollection

    Allows to design collection of warning handlers.
    Allows to get list of warning handles,
    from a specific list of warnings.
    
- WarnHandlerFactory

    Allows to design a warning handler factory, 
    to provide new or specified warning handler instance,
    from specified configuration or file path.
    
_Example_

```php
// Get warning handler factory
use liberty_code\error\warning\handler\factory\model\DefaultWarnHandlerFactory;
$warnHandlerFactory = new DefaultWarnHandlerFactory(null, $provider);
...
// Get new warning handler from configuration
$warnHandler = $warnHandlerFactory->getObjWarnHandler(array(...));
...
// Get new warning handler from handler class file path
$warnHandler = $warnHandlerFactory->getObjWarnHandlerFromFile('...');
...
```

#### Warning builder

Builder allows to hydrate warning handler collection, with warning handlers.

_Elements_

- Builder
    
    Uses array of source data to hydrate warning handler collection.

- DirBuilder
    
    Extends builder features. 
    Uses array of directories paths, 
    to hydrate warning handler collection,
    from each file path, on each directory path.
    
_Example_

```php
// Get warning handler collection
use liberty_code\error\warning\handler\model\DefaultWarnHandlerCollection;
$warnHandlerCollection = new DefaultWarnHandlerCollection();
...
// Get warning handler builder
use liberty_code\error\warning\build\model\DefaultWarnBuilder;
$warnHandlerBuilder = new DefaultWarnBuilder($warnHandlerFactory);
...
// Hydrate warning handler collection
$warnHandlerBuilder->setTabDataSrc(array(...));
$warnHandlerBuilder->hydrateWarnHandlerCollection($warnHandlerCollection);
...
// Get warning handler directory builder
use liberty_code\error\warning\build\directory\DirWarnBuilder;
$handlerDirBuilder = new DirWarnBuilder($warnHandlerFactory);
...
// Hydrate warning handler collection
$warnHandlerBuilder->setTabDataSrc(array(...));
$warnHandlerBuilder->hydrateWarnHandlerCollection($warnHandlerCollection, false);
...
foreach($warnHandlerCollection->getTabKey() as $key) {
    echo($warnHandlerCollection->getObjWarnHandler($key)->getStrKey() .'<br />');
}
/**
 * Show: 
 * Warning handler key 1
 * ...
 * Warning handler key N
 */
...
```

#### Warning tryer

Warning tryer allows to execute specific process 
and handle potential warnings after this process succeeded or failed,
using warning handler collection.

_Elements_

- WarnTryer

    Extends tryer features.
    Allows to design a warning tryer,
    which try execution of specific process
    and handle potential warnings after this process succeeded or failed,
    using warning handler collection.

_Example_

```php
use liberty_code\error\warning\tryer\model\DefaultWarnTryer;
$warnTryer = new DefaultWarnTryer(
    $handlerCollection, 
    $warnHandlerCollection, 
    // Register interface implementation, used to store warnings during specific process
    $warnRegister
);
...
// Try specific process execution
$executionSuccess = false;
$resultExecution = $warnTryer->execute(callable..., $executionSuccess);
...
// Show if specific process succeeded or failed
var_dump($executionSuccess);
...
// Show return from specific process and potential warnings handling if success, 
// from error and potential warnings handling else
var_dump($resultExecution);
...
```

#### Warning error handler

Warning error handlers allows to handle specific errors, considered as warnings,
to execute specific warning action.

_Elements_

- WarnErrorHandler

    Extends error handler features. 
    Execute action by putting error exception, 
    on specific warning register.

- FixWarnErrorHandler

    Extends warning error handler features. 
    Uses specified fixed configuration.
    
- WarnHandlerFactory

    Extends handler factory features. 
    Provides warning error handler instance.

---


